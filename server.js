var express = require('express');
var app = express();
var server = require('http').createServer(app);
var port = process.env.PORT || 3000;
var compress = require('compression');

var bodyParser = require('body-parser');

app.use(compress());
app.use(bodyParser.json({}));
// app.use(bodyParser.urlencoded({, extended: true}));
app.use(bodyParser.json({ limit: '50mb', type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(express.static(__dirname + '/front-end'));

app.set('view engine', 'jade');

var routes = require('./back-end/route');
routes(app);


/////////////////////////
var connect = require('connect');
var serveStatic = require('serve-static');
var vhost = require('vhost');

var mainapp = connect();

// add middlewares to mainapp for the main web site

// create app that will server user content from public/{username}/
var userapp = connect();

userapp.use(function (req, res, next) {
  var username = req.vhost[0]; // username is the "*"

  // pretend request was for /{username}/* for file serving
  req.originalUrl = req.url;
  req.url = '/' + username + req.url;

  next();
})
userapp.use(serveStatic('public'));

// create main app
var app = connect();

server.listen(port);
console.log('Server is running on ' + port);
exports = module.exports = app;
