var fs = require("fs");
var multer = require('multer');
var upload = multer({ dest: '/tmp' })

var vmController = require('../controller/vmController');
var RESPON_SUCCESS = {
  success: true
}

var jsonfile = require('jsonfile');
var file = 'back-end/data/vm/data.json';


module.exports = function (app) {
  /* Get all data */
  app.get('/data-vml', function (req, res) {
    switch (req.headers.host) {
      case 'vml.me:3000':
      case 'vml.me:3002':
      case 'vamiland.com':
      case 'vamihome.com':

        console.log('data-vml');
        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.sendfile('back-end/data/vml/data.json');
        break;
    }
  });

  /* Get all data */
  app.get('/data-vm', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.article) {
            obj.article[index].id *= 1;
          }

          // jsonfile.writeFile(file, obj, function (err) {
          //   console.error(err)
          // });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.sendfile('back-end/data/vm/data.json');
        break;
    }
  });

  /* Project Active*/
  app.post('/update-vm/project/:id/active', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var projectId = req.params.id * 1;
        var projectActive = req.body.active;
        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.article) {
            if (obj.article[index].id * 1 === projectId) {
              obj.article[index].active = projectActive;
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Project Create */

  app.post('/update-vm/project/create', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        jsonfile.readFile(file, function (err, obj) {
          var newId = obj.article[obj.article.length - 1].id;
          newId = newId * 1 + 1;
          var datePosted = new Date();
          datePosted = datePosted.getDate() + '/' + (datePosted.getMonth() * 1 + 1) + '/' + datePosted.getFullYear();
          var newFolder = "./1000/files/documents/" + newId;
          console.log(fs);
          //fs.mkdir(newFolder, function(){});
          var newProject = {
            "id": newId,
            "active": false,
            "hide": false,
            "productType": "home",
            "baseInfo": {
              "name": "Dự án mới chưa có thông tin (Bấm vào chỉnh sửa để cập nhật)",
              "nameCode": "du-an-moi-" + newId,
              "address": "Thông tin địa chỉ dự án",
              "mapBackground": 1,
              "datePosted": datePosted,
              "thumbnail": "./1000/module/app/images/thumbnail.jpg",
              "description": "",
              "price": {
                "value": "0",
                "display": "0 Đồng"
              },
              "promotion": ""
            },
            "tagList": [2],
            "area": {
              "value": "",
              "shortText": ""
            },
            "filterDisplay": [
              {
                "id": 1,
                "value": 1
              },
              {
                "id": 2,
                "value": 1
              },
              {
                "id": 3,
                "value": 1
              },
              {
                "id": 4,
                "value": 1
              },
              {
                "id": 5,
                "value": 1
              },
              {
                "id": 6,
                "value": 1
              }
            ],
            "filterInfo": [
              {
                "key": "nameCode",
                "text": "Mã số",
                "value": ""
              },
              {
                "key": "type",
                "text": "Loại dự án",
                "value": "Nhà phố"
              },
              {
                "key": "juridical",
                "text": "Pháp lý",
                "value": "Số hồng"
              },
              {
                "key": "progress",
                "text": "Tiến độ",
                "value": ""
              },
              {
                "key": "status",
                "text": "Trạng thái",
                "value": "Đang mở bán"
              }
            ],
            "content": "",
            "images": [],
            "download": "",
            "documents": [],
            "keyword": []
          };

          obj.article.push(newProject);

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });

          res.set({ 'content-type': 'application/json; charset=utf-8' });
          res.send({
            success: true,
            newProject: newProject
          });
        });
        break;
    }
  });

  /* News Active */
  app.post('/update-vm/news/:id/active', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var projectId = req.params.id * 1;
        var projectActive = req.body.active;
        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.blog) {
            if (obj.blog[index].id * 1 === projectId) {
              obj.blog[index].active = projectActive;
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* News Create */
  app.post('/update-vm/news/create', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        jsonfile.readFile(file, function (err, obj) {
          var newId = obj.blog[obj.blog.length - 1].id;
          newId = newId * 1 + 1;
          var datePosted = new Date();
          datePosted = datePosted.getDate() + '/' + (datePosted.getMonth() * 1 + 1) + '/' + datePosted.getFullYear();

          var newNews = {
            "id": newId,
            "active": false,
            "blogType": "news",
            "name": "Tin tức mới chưa có thông tin (Bấm vào chỉnh sửa để cập nhật)",
            "nameCode": "tin-tuc-moi-" + newId,
            "thumbnail": "./1000/module/app/images/thumbnail.jpg",
            "description": "",
            "datePosted": datePosted,
            "category": {
              "title": "",
              "name": ""
            },
            "tagList": [
            ],
            "content": [],
            "keyword": []
          };

          obj.blog.push(newNews);

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });

          res.set({ 'content-type': 'application/json; charset=utf-8' });
          res.send({
            success: true,
            newNews: newNews
          });
        });
        break;
    }
  });

  /* Map Active */
  app.post('/update-vm/map/:id/active', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var mapId = req.params.id * 1;
        var projectActive = req.body.active;
        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.map) {
            if (obj.map[index].id * 1 === mapId) {
              obj.map[index].active = projectActive;
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Map Create */
  app.post('/update-vm/map/create', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        jsonfile.readFile(file, function (err, obj) {
          var newId = obj.map[obj.map.length - 1].id;
          newId = newId * 1 + 1;

          var newMap = {
            "id": newId,
            "active": false,
            "title": "Bản đồ mới số " + newId,
            "thumbnail": "./1000/files/images/map/0.jpg"
          };

          obj.map.push(newMap);

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });

          res.set({ 'content-type': 'application/json; charset=utf-8' });
          res.send({
            success: true,
            newMap: newMap
          });
        });
        break;
    }
  });

  /* Map upload */
  app.post('/update-vm/map/:id/upload', upload.single("file"), function (req, res) {
    var mapId = req.params.id * 1;
    var time = new Date();
    var fileType = req.file.mimetype.split('/');
    var fileUpload = __dirname + '/../../front-end/1000/files/images/map/' + time.getTime() + '.' + fileType[1];

    var imageUrl = './1000/files/images/map/' + time.getTime() + '.' + fileType[1];
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(fileUpload, data, function (err) {
        if (err) {
          response = {
            success: false
          };
        } else {
          response = {
            success: true,
            imageUrl: imageUrl
          };
        }

        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.map) {
            if (obj.map[index].id * 1 === mapId) {
              obj.map[index].thumbnail = imageUrl;
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(JSON.stringify(response));
      });
    });
  });

  /* Map Save */
  app.post('/update-vm/map/save', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var map = req.body.map;
        console.log(map);
        jsonfile.readFile(file, function (err, obj) {
          obj.map = map;

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Tag Active */
  app.post('/update-vm/tag/:id/active', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var tagId = req.params.id * 1;
        var projectActive = req.body.active;
        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.tag) {
            if (obj.tag[index].id * 1 === tagId) {
              obj.tag[index].active = projectActive;
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Tag Save */
  app.post('/update-vm/tag/save', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var tag = req.body.tag;
        console.log(tag);
        jsonfile.readFile(file, function (err, obj) {
          obj.tag = tag;

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Tag Create */
  app.post('/update-vm/tag/create', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        jsonfile.readFile(file, function (err, obj) {
          var newId = obj.tag[obj.tag.length - 1].id;
          newId = newId * 1 + 1;

          var newTag = {
            "id": newId,
            "active": false,
            "title": "Thẻ mới số " + newId,
            "color": "red"
          };

          obj.tag.push(newTag);

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });

          res.set({ 'content-type': 'application/json; charset=utf-8' });
          res.send({
            success: true,
            newTag: newTag
          });
        });
        break;
    }
  });

  /* Filter Save */
  app.post('/update-vm/filter/save', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var filter = req.body.filter;
        console.log(filter);
        jsonfile.readFile(file, function (err, obj) {
          obj.filter = filter;

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Image Create */
  app.post('/update-vm/image/create', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        jsonfile.readFile(file, function (err, obj) {
          var newId = obj.generalImage[obj.generalImage.length - 1].id;
          newId = newId * 1 + 1;

          var newImage = {
            "id": newId,
            "active": false,
            "title": "Nhóm hình ảnh mới số " + newId,
            "list": []
          };

          obj.generalImage.push(newImage);

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });

          res.set({ 'content-type': 'application/json; charset=utf-8' });
          res.send({
            success: true,
            newImage: newImage
          });
        });
        break;
    }
  });

  /* Image Active */
  app.post('/update-vm/image/:id/active', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var imageId = req.params.id * 1;
        var projectActive = req.body.active;
        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.generalImage) {
            if (obj.generalImage[index].id * 1 === imageId) {
              obj.generalImage[index].active = projectActive;
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Filter Save */
  app.post('/update-vm/image/save', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var image = req.body.image;
        jsonfile.readFile(file, function (err, obj) {
          obj.generalImage = image;

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Image upload */
  app.post('/update-vm/image/:id/upload', upload.single("file"), function (req, res) {
    var imageId = req.params.id * 1;
    var time = new Date();
    var fileType = req.file.mimetype.split('/');
    var fileUpload = __dirname + '/../../front-end/1000/files/images/general-house/' + time.getTime() + '.' + fileType[1];

    var imageUrl = './1000/files/images/general-house/' + time.getTime() + '.' + fileType[1];
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(fileUpload, data, function (err) {
        if (err) {
          response = {
            success: false
          };
        } else {
          response = {
            success: true,
            imageUrl: imageUrl
          };
        }

        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.generalImage) {
            if (obj.generalImage[index].id * 1 === imageId) {
              obj.generalImage[index].list.push(imageUrl);
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(JSON.stringify(response));
      });
    });
  });

  /* Document Active */
  app.post('/update-vm/document/:id/active', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var documentId = req.params.id * 1;
        var projectActive = req.body.active;
        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.generalDocument) {
            if (obj.generalDocument[index].id * 1 === documentId) {
              obj.generalDocument[index].active = projectActive;
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Document Create */
  app.post('/update-vm/document/create', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        jsonfile.readFile(file, function (err, obj) {
          var newId = obj.generalDocument[obj.generalDocument.length - 1].id;
          newId = newId * 1 + 1;

          var newDocument = {
            "id": newId,
            "active": false,
            "title": "Tài liệu mới số " + newId,
            "src": "",
            "icon": "picture-o"
          };

          obj.generalDocument.push(newDocument);

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });

          res.set({ 'content-type': 'application/json; charset=utf-8' });
          res.send({
            success: true,
            newDocument: newDocument
          });
        });
        break;
    }
  });

  /* Document Save */
  app.post('/update-vm/document/save', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var document = req.body.document;
        jsonfile.readFile(file, function (err, obj) {
          obj.generalDocument = document;

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Document upload */
  app.post('/update-vm/document/:id/upload', upload.single("file"), function (req, res) {
    var documentId = req.params.id * 1;
    var time = new Date();
    var fileType = req.file.mimetype.split('/');
    var fileUpload = __dirname + '/../../front-end/1000/files/documents/general-document/' + time.getTime() + '.' + fileType[1];

    var imageUrl = './1000/files/documents/general-document/' + time.getTime() + '.' + fileType[1];
    var icon = fileType[1] === 'doc' || fileType[1] === 'docx' || fileType[1] === 'xls' || fileType[1] === 'xlsx' || fileType[1] === 'pdf'
      ? 'file-word-o' : 'picture-o';
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(fileUpload, data, function (err) {
        if (err) {
          response = {
            success: false
          };
        } else {
          response = {
            success: true,
            imageUrl: imageUrl,
            icon: icon
          };
        }

        jsonfile.readFile(file, function (err, obj) {
          for (var index in obj.generalDocument) {
            if (obj.generalDocument[index].id * 1 === documentId) {
              obj.generalDocument[index].src = imageUrl;
              obj.generalDocument[index].icon = icon;
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(JSON.stringify(response));
      });
    });
  });

  /* Product save */
  app.post('/update-vm/product/save', function (req, res) {
    switch (req.headers.host) {
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
      case 'vamiland.com':
      case 'vamihome.com':

        var product = req.body.product;
        console.log(product);
        jsonfile.readFile(file, function (err, obj) {
          for (var i = 0; i < obj.article.length; i++) {
            if (obj.article[i].id === product.id) {
              console.log('ok');
              obj.article[i] = product;
            }
          }

          jsonfile.writeFile(file, obj, function (err) {
            console.error(err)
          });
        });

        res.set({ 'content-type': 'application/json; charset=utf-8' });
        res.send(RESPON_SUCCESS);
        break;
    }
  });

  /* Angular app */
  app.get('*', function (req, res) {
    // console.log(req.host.split(':')[0]);
    switch (req.headers.host) {
      case 'web1000.me:3000':
        res.sendfile('front-end/1000/index.html');
        break;
      case 'web1002.me:3000':
        res.sendfile('front-end/1002/index.html');
        break;
      case 'web1004.me:3000':
        res.sendfile('front-end/1004/index.html');
        break;
      case 'web1006.me:3000':
        res.sendfile('front-end/1006/index.html');
        break;
      case 'vml.me:3000':
      case 'vml.me:3002':
      case 'vml.me:3003':
      case 'vml.me:3004':
      case 'vamiland.com':
      case 'vamihome.com':
        res.sendfile('front-end/1008/index.html');
        break;
      case 'web1010.me:3000':
        res.sendfile('front-end/1010/index.html');
        break;


      case 'voducthanh.me:80':
      case 'voducthanh.com':
        res.sendfile('front-end/1002/index.html');
        break;
      case 'frontendvn.net':
        res.sendfile('front-end/1004/index.html');
        break;
      case 'crt-demo.frontendvn.net':
      case 'ctr-demo.frontendvn.net':
        res.sendfile('front-end/1006/index.html');
        break;

      case 'localhost:3000':
      case 'vm.me:3000':
      case 'batdongsanvanminh.me:3000':
      case 'batdongsanvanminh.com':
      case 'thietkexaydungvanminh.com':
        // res.send('Trang web đang đuợc cập nhật nội dung. Vui lòng quay lại trong thời gian tới. Xin cảm ơn!');
        res.sendfile('front-end/1000/index.html');
        // res.sendfile('front-end/1000/page/update/update.view.html');
        break;

      default:
        res.send('domain not setting in system...');
        break;
    }
  });
};
