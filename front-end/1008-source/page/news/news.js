class newsController {
  constructor($rootScope, $scope, $state, $timeout) {
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$state = $state;
    this.$timeout = $timeout;
  }
}

angular
  .module('news', ['ui.router'])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      $stateProvider
        .state('news', {
          url: "/cam-nang-bat-dong-san",
          template: '<news></news>'
        })
    }
  ])
  .directive('news', [() => {
    return {
      restrict: 'E',
      templateUrl: './1008/page/news/news.html',
      transclude: true,
      controller: newsController
    }
  }]);
