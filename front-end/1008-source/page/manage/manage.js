
class manageController {
  constructor($rootScope, $scope, $state, $http, $filter) {
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$state = $state;
    this.$http = $http;

  }
}
angular
  .module('manage', ['ui.router'])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      $stateProvider
        .state('manage', {
          url: "/manage",
          template: '<manage></manage>'
        })
    }
  ])
  .directive('manage', [() => {
    return {
      restrict: 'E',
      templateUrl: './1008/page/manage/manage.html',
      transclude: true,
      controller: manageController
    }
  }]);
