class homeController {
  constructor($rootScope, $scope, $state, $timeout) {
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$state = $state;
    this.$timeout = $timeout;

    this.$scope.data = this.$rootScope.data;
    console.log(this.$scope.data);

    this.$scope.productList = this.$rootScope.data.article.filter(product => false === product.hide);

    this.initMap();
  }

  initMap() {
    var map;
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: new google.maps.LatLng(10.850974, 106.719990),
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style'],
      scrollwheel: false,
      navigationControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
    });

    this.$scope.productList.map((item, $index) => {
      setTimeout(() => {
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(item.location.lat, item.location.lon),
          icon: './1008/style/image/map/blue/' + item.id + '.png',
          map: map,
          animation: google.maps.Animation.DROP
        });
      }, $index * 100);
    });
  }
}

angular
  .module('home', ['ui.router'])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      $stateProvider
        .state('home', {
          url: "/",
          template: '<home></home>'
        })
    }
  ])
  .directive('home', [() => {
    return {
      restrict: 'E',
      templateUrl: './1008/page/home/home.html',
      transclude: true,
      controller: homeController
    }
  }]);
