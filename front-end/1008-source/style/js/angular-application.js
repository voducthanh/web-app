angular.module('webApp', [
  /*  Page */
  'home',
  'news',

  /*  App */
  'app',
  'header',
  'footer'
]);
