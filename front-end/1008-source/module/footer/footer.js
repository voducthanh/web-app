class footerController { }

angular
  .module('footer', [])
  .directive('footer', [() => {
    return {
      restrict: 'E',
      templateUrl: './1008/module/footer/footer.html',
      transclude: true,
      controller: footerController
    }
  }]);
