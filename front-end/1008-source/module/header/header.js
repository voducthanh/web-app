class headerController {
  constructor($rootScope, $scope, $state, $timeout) {
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$state = $state;
    this.$timeout = $timeout;

    this.$scope.openNavigation = false;

    this.$scope.toggleNavigation = () => {
      this.$rootScope.showMenu = this.$scope.openNavigation = !this.$scope.openNavigation;
    }
  }
}

angular
  .module('header', [])
  .directive('header', () => {
    return {
      restrict: 'E',
      templateUrl: './1008/module/header/header.html',
      transclude: true,
      controller: headerController
    }
  })
