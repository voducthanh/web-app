angular.module( '_1000_newsWidget', [] )
  .directive('newsWidget', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/news-widget/news-widget.view.html',
      transclude  : true,
      scope : {
        data : '='
      },
      controller  : function($scope, $state){
        $scope.navigateNewsDetail = function(item) {
          $state.go('newsDetail', {id: item.nameCode});
        }
      }
    }
  });