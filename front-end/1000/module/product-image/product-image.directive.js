angular.module( '_1000_productImage', [] )
  .directive('productImage', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/product-image/product-image.view.html',
      transclude  : true,
      scope : {
        data : '='
      },
      controller  : function($scope, fancyboxService){
        $scope.configGallery = {
          'padding': 0,
          'transitionIn': 'none',
          'transitionOut': 'none',
          'type': 'image',
          'overlayOpacity': '0.8',
          'overlayColor': '#000'
        };

        $scope.openGallery = function(){
          var gallery = [];
          for(sub in $scope.data) {
            for(index in $scope.data[sub].list) {
              gallery.push($scope.data[sub].list[index]);
            }
          }
          fancyboxService.fancyboxPlus()(gallery, $scope.configGallery);
        }
      }
    }
  });