angular.module( '_1000_menuMain', [] )
  .directive('menuMain', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/menu-main/menu-main.view.html',
      transclude  : true,
      controller  : function($scope){

        /*  Open Menu Left */
        $scope.openMenuLeft = function() {
          $scope.$emit('toggleMenuLeft', { message: true });
        }

        /*  Open Menu Right */
        $scope.openMenuRight = function() {
          $scope.$emit('toggleMenuRight', { message: true });
        }
      }
    }
  });