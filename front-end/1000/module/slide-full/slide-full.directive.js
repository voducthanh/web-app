angular.module( '_1000_slideFull', [] )
  .directive('slideFull', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/slide-full/slide-full.view.html',
      transclude  : true,
      controller  : function($scope){

        $scope.listSlide = [
          {
            img: './1000/module/slide-full/images/bg.jpg',
            link: '/chi-tiet-du-an/nha-pho-can-so-2-1-tret-2-lau-duong-so-8-hiep-binh-chanh-thu-duc-gia-3-ty',
            title: 'Nhà phố mới xây 1 Trệt, 2 Lầu, đuờng số 8, Hiệp Bình Chánh, Thủ Đức, Tp. HCM',
            description: 'Giá từ 3,00 Tỷ',
            textButton: 'Xem chi tiết'
          },
          {
            img: './1000/module/slide-full/images/002.jpg',
            link: '#',
            title: 'Biệt thự Linh Trung Thủ Đức',
            description: 'Giá từ 10 Tỷ',
            textButton: 'Xem chi tiết'
          },
          {
            img: './1000/module/slide-full/images/003.jpg',
            link: '#',
            title: 'Biệt thự Linh Trung Thủ Đức',
            description: 'Giá từ 10 Tỷ',
            textButton: 'Xem chi tiết'
          }
        ];

        $scope.slideSelected = $scope.listSlide[0];
      }
    }
  });