angular.module( '_1000_newsItem', [] )
  .directive('newsItem', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/news-item/news-item.view.html',
      transclude  : true,
      scope : {
        data : '='
      },
      controller  : function($scope, $state){
        /* navigateToDetail */
        $scope.navigateToDetail = function () {
          $state.go('newsDetail', {id: $scope.data.nameCode});
        }
      }
    }
  });