angular.module( '_1000_productItemList', [] )
  .directive('productItemList', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/product-item-list/product-item-list.view.html',
      transclude  : true,
      scope : {
        data : '='
      },
      controller  : function($scope, $state){
        $scope.filterList = [];
        var dataFilter = {};
        for(index in $scope.data.filterDisplay) {
          switch($scope.data.filterDisplay[index].key) {
            case 'area':
              dataFilter = {};
              dataFilter = $scope.data.filterDisplay[index];
              dataFilter.icon = "arrows-alt";
              dataFilter.value = $scope.data.filterDisplay[index].value;
              $scope.filterList.push(dataFilter);
              break;

            case 'direction':
              dataFilter = {};
              dataFilter.icon = "compass";
              dataFilter.value = "Huớng " + $scope.data.filterDisplay[index].value;
              $scope.filterList.push(dataFilter);
              break;

            case 'bedRoom':
              dataFilter = {};
              dataFilter.icon = "bed";
              dataFilter.value = $scope.data.filterDisplay[index].value + " Phòng ngủ";
              $scope.filterList.push(dataFilter);
              break;

            default: break;
          }
        }

        /* navigateToDetail */
        $scope.navigateToDetail = function () {
          $state.go('productDetail', {id: $scope.data.baseInfo.nameCode});
        }
      }
    }
  });