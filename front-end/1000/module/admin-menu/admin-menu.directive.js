angular.module( '_1000_adminMenu', [] )
  .directive('adminMenu', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/admin-menu/admin-menu.view.html',
      transclude  : true,
      controller  : function($scope){
      }
    }
  });