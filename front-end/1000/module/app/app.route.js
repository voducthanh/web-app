_1000_app.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    /*  404 */
    $urlRouterProvider.otherwise("/");

    /*Thiết lập các state*/
    $stateProvider
    .state('home', {
      url: "/",
      templateUrl: './1000/page/home/home.view.html',
      controller: 'homeController'
    })

    .state('product', {
      url: "/danh-sach-du-an",
      templateUrl: './1000/page/product/product.view.html',
      controller: 'productController'
    })

    .state('productDetail', {
      url: "/chi-tiet-du-an/:id",
      templateUrl: './1000/page/product-detail/product-detail.view.html',
      controller: 'productDetailController'
    })

    .state('introduction', {
      url: "/gioi-thieu",
      templateUrl: './1000/page/news-detail/introduction.view.html',
      controller: 'introductionController'
    })

    .state('recruitment', {
      url: "/tuyen-dung",
      templateUrl: './1000/page/news-detail/recruitment.view.html',
      controller: 'recruitmentController'
    })

    .state('contact', {
      url: "/lien-he",
      templateUrl: './1000/page/news-detail/contact.view.html',
      controller: 'contactController'
    })

    .state('news', {
      url: "/tin-tuc",
      templateUrl: './1000/page/news/news.view.html',
      controller: 'newsController'
    })

    .state('newsDetail', {
      url: "/tin-tuc-chi-tiet/:id",
      templateUrl: './1000/page/news-detail/news-detail.view.html',
      controller: 'newsDetailController'
    })

    .state('login', {
      url: "/login",
      templateUrl: './1000/page/login/login.view.html',
      controller: 'loginController'
    })

    .state('dashboard', {
      url: "/quan-ly",
      templateUrl: './1000/page/dashboard/dashboard.view.html',
      controller: 'dashboardController'
    })

    .state('adminProductList', {
      url: "/quan-ly/du-an",
      templateUrl: './1000/page/admin-product-list/admin-product-list.view.html',
      controller: 'adminProductListController'
    })

    .state('adminProductDetail', {
      url: "/quan-ly/du-an/:id",
      templateUrl: './1000/page/admin-product-detail/admin-product-detail.view.html',
      controller: 'adminProductDetailController'
    })

    .state('adminNewsList', {
      url: "/quan-ly/tin-tuc",
      templateUrl: './1000/page/admin-news-list/admin-news-list.view.html',
      controller: 'adminNewsListController'
    })

    .state('adminMap', {
      url: "/quan-ly/ban-do",
      templateUrl: './1000/page/admin-map/admin-map.view.html',
      controller: 'adminMapController'
    })

    .state('adminTag', {
      url: "/quan-ly/the",
      templateUrl: './1000/page/admin-tag/admin-tag.view.html',
      controller: 'adminTagController'
    })

    .state('adminFilter', {
      url: "/quan-ly/bo-loc",
      templateUrl: './1000/page/admin-filter/admin-filter.view.html',
      controller: 'adminFilterController'
    })

    .state('adminImage', {
      url: "/quan-ly/hinh-anh-chung",
      templateUrl: './1000/page/admin-image/admin-image.view.html',
      controller: 'adminImageController'
    })

    .state('adminDocument', {
      url: "/quan-ly/tai-lieu-chung",
      templateUrl: './1000/page/admin-document/admin-document.view.html',
      controller: 'adminDocumentController'
    })

    .state('404', {
      url: "/404",
      templateUrl: './1000/page/404/404.view.html',
      title: '404 - Không tìm thấy trang này'
    });

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });

    $locationProvider.hashPrefix('!');
  }
]);