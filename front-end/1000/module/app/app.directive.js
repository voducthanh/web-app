var _1000_app = angular.module( '_1000_app', ['ui.router', 'ngSanitize'] );

_1000_app.directive('app', function() {
  return {
    restrict: 'E',
    templateUrl : './1000/module/app/app.view.html',
    transclude  : true,
    controller  : function($scope, $rootScope, $state, $http){
      /*  Toogle Menu Left */
      $scope.showMenuLeft = false;
      $scope.showMenuRight = false;
      $scope.hideMenu = false;

      $scope.$on('toggleMenuLeft', function(event, args){
        $scope.showMenuLeft = args.message;
      });

      $scope.$on('toggleMenuRight', function(event, args){
        $scope.showMenuRight = args.message;
      });

      $scope.$on('hideMenu', function(event, args){
        $scope.hideMenu = args.message;
      });

      /* Authorize admin page */
      $scope.$on('byPassLogin', function(event, args){
        var auth = localStorage.getItem('auth');
        if (auth === 'admin') {
          $state.go('dashboard');
        }
      });

      $scope.$on('checkAuth', function(event, args){
        var auth = localStorage.getItem('auth');
        if (auth !== 'admin') {
          $state.go('login');
        }
      });
    }
  }
});

/*

[
  {
    "featureType": "administrative",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#444444"
      }
    ]
  },
  {
    "featureType": "landscape",
    "stylers": [
      {
        "color": "#f2f2f2"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "stylers": [
      {
        "saturation": -100
      },
      {
        "lightness": 45
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#d7dbd6"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "stylers": [
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#dce0df"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#ff2321"
      },
      {
        "visibility": "simplified"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "labels.text",
    "stylers": [
      {
        "color": "#6dff94"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#dce0df"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#696a68"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#51ff53"
      }
    ]
  },
  {
    "featureType": "water",
    "stylers": [
      {
        "color": "#3687d4"
      },
      {
        "visibility": "on"
      }
    ]
  }
]

*/