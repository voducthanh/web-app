angular.module( '_1000_keywordWidget', [] )
  .directive('keywordWidget', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/keyword-widget/keyword-widget.view.html',
      transclude  : true,
      controller  : function($scope){
        $scope.keywordList = [
          {
            title: 'Nhà (50)',
            href: '#'
          },
          {
            title: 'Nhà đẹp (40)',
            href: '#'
          },
          {
            title: 'Tư vấn (36)',
            href: '#'
          },
          {
            title: 'Sơn nhà (34)',
            href: '#'
          },
          {
            title: 'Đất (34)',
            href: '#'
          },
          {
            title: 'Ngoại thất (26)',
            href: '#'
          },
          {
            title: 'Phong thuỷ (20)',
            href: '#'
          },
          {
            title: 'Nhà phố (20)',
            href: '#'
          },
          {
            title: 'Chưng cư (18)',
            href: '#'
          },
          {
            title: 'Đất chính chủ (15)',
            href: '#'
          },
          {
            title: 'Mua nhà (10)',
            href: '#'
          }
        ]
      }
    }
  });