angular.module( '_1000_menuLeft', [] )
  .directive('menuLeft', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/menu-left/menu-left.view.html',
      transclude  : true,
      controller  : function($scope, $state){
        /*  Data filter*/
        $scope.listFilter = [
          {
            heading: 'Loại Dự án',
            value: '',
            list: [
              {
                key: 'option-1-1',
                value: 'Nhà phố'
              },
              {
                key: 'option-1-2',
                value: 'Biệt thự'
              },
              {
                key: 'option-1-3',
                value: 'Đất nền'
              }
            ]
          },
          {
            heading: 'Mức giá',
            value: '',
            list: [
              {
                key: 'option-2-1',
                value: 'Duới 1 Tỷ'
              },
              {
                key: 'option-2-2',
                value: '1 Tỷ - 2 Tỷ'
              },
              {
                key: 'option-2-3',
                value: '2 Tỷ - 3 Tỷ'
              },
              {
                key: 'option-2-4',
                value: '3 Tỷ - 5 Tỷ'
              },
              {
                key: 'option-2-5',
                value: 'Trên 5 Tỷ'
              }
            ]
          },
          {
            heading: ' Khu vực',
            value: '',
            list: [
              {
                key: 'option-3-1',
                value: 'Đuờng số 3'
              },
              {
                key: 'option-3-2',
                value: 'Đuờng số 8'
              },
              {
                key: 'option-3-3',
                value: 'Đuờng số 9'
              },
              {
                key: 'option-3-4',
                value: 'Hẻm 647'
              }
            ]
          },
          {
            heading: 'Diện tích',
            value: '',
            list: [
              {
                key: 'option-4-1',
                value: '50 m2 - 60m2'
              },
              {
                key: 'option-4-2',
                value: '60 m2 - 80m2'
              },
              {
                key: 'option-4-3',
                value: '80 m2- 100m2'
              },
              {
                key: 'option-4-4',
                value: 'Trên 100m2'
              }
            ]
          }
        ];

        /*  Close Menu Left */
        $scope.closeMenuLeft = function () {
          $scope.$emit('toggleMenuLeft', { message: false });
        }

        /* navigateToHome */
        $scope.navigateToHome = function() {
          $state.go('home');
          $scope.$emit('toggleMenuLeft', { message: false });
        }

        /* navigateToProduct */
        $scope.navigateToProduct = function() {
          $state.go('product');
          $scope.$emit('toggleMenuLeft', { message: false });
        }
      }
    }
  });