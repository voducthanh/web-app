angular.module( '_1000_newsList', [] )
  .directive('newsList', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/news-list/news-list.view.html',
      transclude  : true,
      controller  : function($rootScope, $scope, $state){

        $scope.showPagenation = false;

        if($state.current.name === 'news') {
          $scope.showPagenation = true;
        }

        /* Init data */
        $scope.dataNews = $rootScope.data.blog;

        switch($state.current.name) {
          case 'home':
            $scope.titleNewsList = 'Tin tức';
            break;

          case 'news':
            $scope.titleNewsList = 'Tin tức mới';
            break;
        }

        $scope.checkShow = function(index) {
          if ('news' === $state.current.name) {
            return true;
          }

          return index <= 5;
        }
      }
    }
  });