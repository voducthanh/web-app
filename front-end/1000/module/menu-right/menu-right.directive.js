angular.module( '_1000_menuRight', [] )
  .directive('menuRight', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/menu-right/menu-right.view.html',
      transclude  : true,
      controller  : function($scope){

        /*  Close Menu Right */
        $scope.closeMenuRight = function () {
          $scope.$emit('toggleMenuRight', { message: false });
        }
      }
    }
  });