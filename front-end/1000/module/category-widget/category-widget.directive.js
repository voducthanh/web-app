angular.module( '_1000_categoryWidget', [] )
  .directive('categoryWidget', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/category-widget/category-widget.view.html',
      transclude  : true,
      controller  : function($scope){
        $scope.categoryList = [
          {
            title: 'Kiến trúc',
            href: '#'
          },
          {
            title: 'Xây dựng',
            href: '#'
          },
          {
            title: 'Nội thất',
            href: '#'
          },
          {
            title: 'Ngoại thất',
            href: '#'
          },
          {
            title: 'Phong thuỷ',
            href: '#'
          }
        ]
      }
    }
  });