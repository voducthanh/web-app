angular.module( '_1000_productItem', [] )
  .directive('productItem', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/product-item/product-item.view.html',
      transclude  : true,
      scope : {
        data : '='
      },
      controller  : function($rootScope, $scope, $state){

        /* List tag data */
        $scope.listTagProduct = [];
        for(index in $scope.data.tagList) {
          for(indexSub in $rootScope.data.tag) {
            if ($scope.data.tagList[index] * 1 === $rootScope.data.tag[indexSub].id * 1) {
              $scope.listTagProduct.push($rootScope.data.tag[indexSub]);
            }
          }
        }

        $scope.filterListProduct = [];
        var dataFilter = {};
        for(index in $scope.data.filterDisplay) {
          for(indexSub in $rootScope.data.filter) {
            var temp = {};
            if ($scope.data.filterDisplay[index].id * 1 === $rootScope.data.filter[indexSub].id * 1
                && (
                  $rootScope.data.filter[indexSub].key === 'direction'
                  || $rootScope.data.filter[indexSub].key === 'bedRoom'
                  )
                ) {
              temp.title = $rootScope.data.filter[indexSub].title;
              temp.icon = $rootScope.data.filter[indexSub].icon;
              temp.preTitle = $rootScope.data.filter[indexSub]['pre-title'];
              temp.posTitle = $rootScope.data.filter[indexSub]['pos-title'];

              for(indexList in $rootScope.data.filter[indexSub].list) {
                if ($scope.data.filterDisplay[index].value === $rootScope.data.filter[indexSub].list[indexList].id) {
                  temp.value = $rootScope.data.filter[indexSub].list[indexList].value;
                }
              }

              $scope.filterListProduct.push(temp);
            }
          }
        }

        /* navigateToDetail */
        $scope.navigateToDetail = function () {
          $state.go('productDetail', {id: $scope.data.baseInfo.nameCode});
        }
      }
    }
  });