angular.module( '_1000_breadcrumb', [] )
  .directive('breadcrumb', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/breadcrumb/breadcrumb.view.html',
      transclude  : true,
      scope : {
        list : '='
      },
      controller  : function($scope){
        console.log($scope.list)
      }
    }
  });