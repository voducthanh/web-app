angular.module( '_1000_widget', [] )
  .directive('widget', function() {
    return {
      restrict: 'E',
      transclude  : {
        'heading': '?widgetHeading',
        'content': '?widgetContent'
      },
      template: [
          '<div ng-transclude="heading"></div>',
          '<div ng-transclude="content"></div>',
        ].join(''),
      controller  : function($scope){
      }
    }
  });