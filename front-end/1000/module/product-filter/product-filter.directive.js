angular.module( '_1000_productFilter', [] )
  .directive('productFilter', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/product-filter/product-filter.view.html',
      transclude  : true,
      controller  : function($scope){
        $scope.filterRadio = 'All';

        /*  Option show list project */
        $scope.listType = localStorage.getItem('list-type-show-product');

        /*  Change Type view */
        $scope.changeTypeView = function() {
          if ($scope.listType == 'grid') {
            $scope.listType = 'list';
          } else {
            $scope.listType = 'grid';
          }

          $scope.$emit('changeTypeShowProduct', { message: $scope.listType });
        }
      }
    }
  });