angular.module( '_1000_productList', [] )
  .directive('productList', function() {
    return {
      restrict: 'E',
      templateUrl : './1000/module/product-list/product-list.view.html',
      transclude  : true,
      controller  : function($rootScope, $scope, $state){

        /* Init data */
        $scope.dataProduct = [];
        for (var i = 0; i < $rootScope.data.article.length; i++) {
          //$scope.dataProduct.push($rootScope.data.article[i]);
          if ($rootScope.data.article[i].hide === false) {
            $scope.dataProduct.push($rootScope.data.article[i]);
          }
        }

        switch($state.current.name) {
          case 'home':
            $scope.showProductFilter = false;
            $scope.showPagenation = false;

            $scope.titleProductList = 'Dự án Đang mở bán';

            $scope.listType = 'grid';
            break;

          case 'product':
            $scope.showProductFilter =  true;
            $scope.showPagenation = true;
            $scope.titleProductList = 'Dự án Đang mở bán';

            /*  Option show list project */
            $scope.listType = localStorage.getItem('list-type-show-product');
            if ($scope.listType === null) {
              $scope.listType = 'grid';
              localStorage.setItem('list-type-show-product', $scope.listType);
            }
            break;
        }

        /*  Change Type Show Product List */
        $scope.$on('changeTypeShowProduct', function(event, args){
          $scope.listType = args.message;
          localStorage.setItem('list-type-show-product', $scope.listType);
        });

        $scope.checkShow = function(index) {
          if ('product' === $state.current.name) {
            return true;
          }

          return index <= 5;
        }
      }
    }
  });