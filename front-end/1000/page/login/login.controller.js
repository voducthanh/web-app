var _1000_page_login = angular.module( '_1000_page_login', [] );

_1000_page_login.controller('loginController', function($scope, $mdDialog){
  /* Auto by pass */
  $scope.$emit('byPassLogin');

  $scope.$emit('hideMenu', { message: true });

  $scope.login = {
    username: '',
    password: ''
  };

  /* Submit */
  $scope.submit = function() {
    if ($scope.login.username === 'vanminh' && $scope.login.password === 'vm1975') {
      localStorage.setItem('auth', 'admin');
      $scope.$emit('byPassLogin');
    } else {
      $mdDialog.show(
        $mdDialog.alert()
          .clickOutsideToClose(true)
          .textContent('Tài khoản hoặc mật khẩu không chính xác')
          .ok('Đóng')
      );
    }
  }

  $scope.disableButton = function() {
    return $scope.login.username === ''
      || $scope.login.password === '';
  }

});
