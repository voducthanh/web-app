var _1000_page_admin_product_detail = angular.module( '_1000_page_admin_product_detail', [] );

_1000_page_admin_product_detail.controller('adminProductDetailController',
  function($rootScope, $scope, $mdDialog, $http, $state, $stateParams){
    /* check auth */
    $scope.$emit('checkAuth');

    $scope.$emit('hideMenu', { message: true });

    /* Init data */
    var productId = $stateParams.id * 1;
    for(index in $rootScope.data.article) {
      if ($rootScope.data.article[index].id * 1 === productId) {
        $scope.product = $rootScope.data.article[index];
      }
    }

    $scope.mapList = $rootScope.data.map;
    $scope.tagList = $rootScope.data.tag;
    $scope.filterList = $rootScope.data.filter;

    console.log($scope.product);

    /* Get map thumbnail */
    $scope.getMapThumbnail = function() {
      for (index in $scope.mapList) {
        if ($scope.mapList[index].id * 1 === $scope.product.baseInfo.mapBackground * 1) {
          return {"background-image": "url('" + $scope.mapList[index].thumbnail + "')"};
        }
      }
    }

    /* check tag */
    $scope.checkTag = function(tag) {
      return $scope.product.tagList.indexOf(tag.id) >= 0;
    }

    /* select tag */
    $scope.selectTag = function(tag) {
      var indexTag = $scope.product.tagList.indexOf(tag.id);
      if (indexTag >= 0) {
        $scope.product.tagList.splice(indexTag, 1);
      } else {
        $scope.product.tagList.push(tag.id);
      }

      console.log($scope.product.tagList);
    }

    /* Get filter name */
    $scope.getFilterName = function(filter) {
      for (index in $scope.filterList) {
        if ($scope.filterList[index].id * 1 === filter.id * 1) {
          return $scope.filterList[index].title;
        }
      }
    }

    $scope.getFilterList = function(filter) {
      for (index in $scope.filterList) {
        if ($scope.filterList[index].id * 1 === filter.id * 1) {
          return $scope.filterList[index].list;
        }
      }
    }

    $scope.formartNumberLocale = function(number) {
      var _number = '';
      var _count = 1;
      while (number > 0){
        _number = (number % 10) + _number;
        number = (number - (number % 10)) / 10;
        if (_count === 3 & number > 0) {
          _number =',' + _number;
          _count = 1;
        } else {
          _count += 1;
        }
      }
      return _number;
    }

    $scope.saveProduct = function () {
      console.log($scope.product);
      $http({
        method: 'POST',
        url: `/update-vm/product/save`,
        data: {
           product: $scope.product
        }
      }).then(
        function(response) {
          $mdDialog.show(
            $mdDialog.alert()
              .clickOutsideToClose(true)
              .textContent(`Lưu thông tin thành công`)
              .ok('Đóng')
            );
        },function(response) {
          window.location.reload();
        }
      );
    }
  }
).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
