var _1000_page_admin_map = angular.module( '_1000_page_admin_map', [] );

_1000_page_admin_map.controller('adminMapController',
  function($rootScope, $scope, $mdDialog, $http, $state){
    /* check auth */
    $scope.$emit('checkAuth');

    $scope.$emit('hideMenu', { message: true });

    /* Init data */
    $scope.dataMap = $rootScope.data.map;

    /* Change active map */
    $scope.changeActive = function (map) {
      $http({
        method: 'POST',
        url: `/update-vm/map/${map.id}/active`,
        data: {
          active: !map.active
        }
      }).then(
        function(response) {
        },function(response) {
          window.location.reload();
        }
      );
    }

    /* Add new map */
    $scope.addNewMap = function () {
      $http({
        method: 'POST',
        url: '/update-vm/map/create'
      }).then(
        function(response) {
          $scope.dataMap.push(response.data.newMap);
          setTimeout(function() {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .textContent(`Bản đồ mới đuợc tạo thành công với mã số: ${response.data.newMap.id}.`)
                .ok('Đóng')
            );
          }, 1000);

          $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        },function(response) {
          window.location.reload();
        }
      );
    }

    /* Save map info */
    $scope.saveMap = function () {
      $http({
        method: 'POST',
        url: `/update-vm/map/save`,
        data: {
           map: $scope.dataMap
        }
      }).then(
        function(response) {
          $mdDialog.show(
            $mdDialog.alert()
              .clickOutsideToClose(true)
              .textContent(`Lưu thông tin thành công`)
              .ok('Đóng')
            );
        },function(response) {
          window.location.reload();
        }
      );
    }

    $scope.submit = function() {
      setTimeout(function(){
        var fd = new FormData();
        fd.append("file", $scope.myFile);

        $http.post(`/update-vm/map/${$scope.waitToUpload.id}/upload`, fd, {
          withCredentials: false,
          headers: {'Content-Type': undefined},
          transformRequest: angular.identity,
          params: { fd },
        })
        .success(function(response, status, headers, config) {
          $scope.waitToUpload.thumbnail = response.imageUrl;
        })
        .error(function(error, status, headers, config) {
        });
      }, 500);
    }

    $scope.upload = function(map) {
      $scope.waitToUpload = map;
      $('#inputUpload').click();
    }
  }
).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
