var _1000_page_admin_product_list = angular.module( '_1000_page_admin_product_list', [] );

_1000_page_admin_product_list.controller('adminProductListController',
  function($rootScope, $scope, $mdDialog, $http, $state){
    /* check auth */
    $scope.$emit('checkAuth');

    $scope.$emit('hideMenu', { message: true });

    /* Init data */
    $scope.dataProduct = $rootScope.data.article;

    $scope.changeActive = function(product) {
      $http({
        method: 'POST',
        url: `/update-vm/project/${product.id}/active`,
        data: {
          active: !product.active
        }
      }).then(
        function(response) {
        },function(response) {
          window.location.reload();
        }
      );
    }

    $scope.addNewProject = function() {
      $http({
        method: 'POST',
        url: '/update-vm/project/create'
      }).then(
        function(response) {
          $scope.dataProduct.push(response.data.newProject);

          setTimeout(function() {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .textContent(`Dự án mới đuợc tạo thành công với mã số: ${response.data.newProject.id}. Bấm vào chỉnh sửa để cập nhật thông tin`)
                .ok('Đóng')
            );
          }, 1000);

          $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        },function(response) {
          window.location.reload();
        }
      );
    }

    $scope.viewDetail = function(product) {
      $state.go('productDetail', {id: product.baseInfo.nameCode});
    }

    $scope.editDetail = function(product) {
      $state.go('adminProductDetail', {id: product.id});
    }
  }
);
