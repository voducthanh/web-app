var _1000_page_news_detail = angular.module( '_1000_page_news_detail', [] );

_1000_page_news_detail.controller('newsDetailController', ['$rootScope', '$scope', '$stateParams', '$state',
  function($rootScope, $scope, $stateParams, $state){
    $("html, body").animate({ scrollTop: "0" });

    var nameCodeNews = $stateParams.id;
    for(index in $rootScope.data.blog) {
      if ($rootScope.data.blog[index].nameCode === nameCodeNews) {
        $scope.data = $rootScope.data.blog[index];
        break;
      }
    }

    /* Bread crumb */
    $scope.listBreadcrumb = [
      {
        title: 'Tin tức',
        link: '/tin-tuc'
      },
      {
        title: $scope.data.category.title,
        link: '/tin-tuc'
      }
    ];

    /* Render html */
    $scope.renderContent = function(content) {
      switch (content.type) {
        case 'content':
          return ['<p class="', content.class ,'">', content.content, '</p>'].join('');
        case 'image':
          return [
            '<img src="', content.src, '" alt="', content.alt, '"/>',
            '<div class="img-caption">', content.alt, '</div>'
          ].join('');
          return;
      }
    }

    /* News Related */
    $scope.newsTopList = [];
    var rand;
    for(i = 0; i < 7; i++) {
      rand = Math.floor((Math.random() * $rootScope.data.blog.length - 1) + 1);
      $scope.newsTopList.push($rootScope.data.blog[rand]);
    }

    /* Related */
    $scope.productRelated = [];
    var rand;
    for(i = 0; i < 7; i++) {
      rand = Math.floor((Math.random() * $rootScope.data.article.length - 1) + 1);
      $scope.productRelated.push($rootScope.data.article[rand]);
    }

    $scope.navigateRelated = function (related) {
      $state.go('productDetail', {id: related.baseInfo.nameCode});
    }
  }
]);

_1000_page_news_detail.controller('introductionController', ['$scope', function($scope){
    $("html, body").animate({ scrollTop: "0" });
  }
]);

_1000_page_news_detail.controller('recruitmentController', ['$scope', function($scope){
    $("html, body").animate({ scrollTop: "0" });
  }
]);

_1000_page_news_detail.controller('contactController', ['$scope', function($scope){
    $("html, body").animate({ scrollTop: "0" });
  }
]);
