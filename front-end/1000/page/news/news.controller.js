var _1000_page_news = angular.module( '_1000_page_news', [] );

_1000_page_news.controller('newsController', ['$rootScope', '$scope', '$state',
  function($rootScope, $scope, $state){

    $scope.listBreadcrumb = [
      {
        title: 'Tin tức',
        link: '/tin-tuc'
      },
      {
        title: 'Tin tức Bất động sản',
        link: '/tin-tuc'
      }
    ];

    /* News Related */
    $scope.newsTopList = [];
    var rand;
    for(i = 0; i < 7; i++) {
      rand = Math.floor((Math.random() * $rootScope.data.blog.length - 1) + 1);
      $scope.newsTopList.push($rootScope.data.blog[rand]);
    }

    /* Product Related */
    $scope.productRelated = [];
    var rand;
    for(i = 0; i < 7; i++) {
      rand = Math.floor((Math.random() * $rootScope.data.article.length - 1) + 1);
      $scope.productRelated.push($rootScope.data.article[rand]);
    }

    $scope.navigateRelated = function (related) {
      $state.go('productDetail', {id: related.baseInfo.nameCode});
    }
}]);