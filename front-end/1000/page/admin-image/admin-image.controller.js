var _1000_page_admin_image = angular.module( '_1000_page_admin_image', [] );

_1000_page_admin_image.controller('adminImageController',
  function($rootScope, $scope, $mdDialog, $http, $state){
    /* check auth */
    $scope.$emit('checkAuth');

    $scope.$emit('hideMenu', { message: true });

    /* Init data */
    $scope.dataImage = $rootScope.data.generalImage;

    /* Change active map */
    $scope.changeActive = function (image) {
      $http({
        method: 'POST',
        url: `/update-vm/image/${image.id}/active`,
        data: {
          active: !image.active
        }
      }).then(
        function(response) {
        },function(response) {
          window.location.reload();
        }
      );
    }

    /* Add group image */
    $scope.addNewImageGroup = function() {
      $http({
        method: 'POST',
        url: `/update-vm/image/create`
      }).then(
        function(response) {
          $scope.dataImage.push(response.data.newImage);
          setTimeout(function() {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .textContent(`Nhóm hình ảnh mới đuợc tạo thành công với mã số: ${response.data.newImage.id}.`)
                .ok('Đóng')
            );
          }, 1000);

          $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        },function(response) {
          window.location.reload();
        }
      );
    }

    /* Save image */
    $scope.saveImage = function() {
      $http({
        method: 'POST',
        url: `/update-vm/image/save`,
        data: {
           image: $scope.dataImage
        }
      }).then(
        function(response) {
          $mdDialog.show(
            $mdDialog.alert()
              .clickOutsideToClose(true)
              .textContent(`Lưu thông tin thành công`)
              .ok('Đóng')
            );
        },function(response) {
          window.location.reload();
        }
      );
    }

    /* Upload image */
    $scope.submit = function() {
      setTimeout(function(){
        var fd = new FormData();
        fd.append("file", $scope.myFile);

        $http.post(`/update-vm/image/${$scope.waitToUpload.id}/upload`, fd, {
          withCredentials: false,
          headers: {'Content-Type': undefined},
          transformRequest: angular.identity,
          params: { fd },
        })
        .success(function(response, status, headers, config) {
          $scope.waitToUpload.list.push(response.imageUrl);
        })
        .error(function(error, status, headers, config) {
        });
      }, 500);
    }

    $scope.uploadImage = function(image) {
      $scope.waitToUpload = image;
      $('#inputUpload').click();
    }

    /* Delete image */
    $scope.delete = function(image, item) {
      image.list.splice(image.list.indexOf(item), 1);
      $http({
        method: 'POST',
        url: `/update-vm/image/save`,
        data: {
           image: $scope.dataImage
        }
      })
    }
  }
)
