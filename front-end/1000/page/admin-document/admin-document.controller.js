var _1000_page_admin_document = angular.module( '_1000_page_admin_document', [] );

_1000_page_admin_document.controller('adminDocumentController',
  function($rootScope, $scope, $mdDialog, $http, $state){
    /* check auth */
    $scope.$emit('checkAuth');

    $scope.$emit('hideMenu', { message: true });

    /* Init data */
    $scope.dataDocument = $rootScope.data.generalDocument;

    /* Change active document */
    $scope.changeActive = function (document) {
      $http({
        method: 'POST',
        url: `/update-vm/document/${document.id}/active`,
        data: {
          active: !document.active
        }
      }).then(
        function(response) {
        },function(response) {
          window.location.reload();
        }
      );
    }

    /* Add new document */
    $scope.addNewDocument = function () {
      $http({
        method: 'POST',
        url: '/update-vm/document/create'
      }).then(
        function(response) {
          $scope.dataDocument.push(response.data.newDocument);
          setTimeout(function() {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .textContent(`Tài liệu mới đuợc tạo thành công với mã số: ${response.data.newDocument.id}.`)
                .ok('Đóng')
            );
          }, 1000);

          $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        },function(response) {
          window.location.reload();
        }
      );
    }

    /* Save mdocumentap info */
    $scope.saveDocument = function () {
      $http({
        method: 'POST',
        url: `/update-vm/document/save`,
        data: {
           document: $scope.dataDocument
        }
      }).then(
        function(response) {
          $mdDialog.show(
            $mdDialog.alert()
              .clickOutsideToClose(true)
              .textContent(`Lưu thông tin thành công`)
              .ok('Đóng')
            );
        },function(response) {
          window.location.reload();
        }
      );
    }

    $scope.submit = function() {
      setTimeout(function(){
        var fd = new FormData();
        fd.append("file", $scope.myFile);

        $http.post(`/update-vm/document/${$scope.waitToUpload.id}/upload`, fd, {
          withCredentials: false,
          headers: {'Content-Type': undefined},
          transformRequest: angular.identity,
          params: { fd },
        })
        .success(function(response, status, headers, config) {
          $scope.waitToUpload.src = response.imageUrl;
        })
        .error(function(error, status, headers, config) {
        });
      }, 500);
    }

    $scope.upload = function(document) {
      $scope.waitToUpload = document;
      $('#inputUpload').click();
    }
  }
).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
