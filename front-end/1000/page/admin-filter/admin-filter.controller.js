var _1000_page_admin_filter = angular.module( '_1000_page_admin_filter', [] );

_1000_page_admin_filter.controller('adminFilterController',
  function($rootScope, $scope, $mdDialog, $http, $state){
    /* check auth */
    $scope.$emit('checkAuth');

    $scope.$emit('hideMenu', { message: true });

    /* Init data */
    $scope.dataFilter = $rootScope.data.filter;

    /* Filter map info */
    $scope.saveFilter = function () {
      $http({
        method: 'POST',
        url: `/update-vm/filter/save`,
        data: {
           filter: $scope.dataFilter
        }
      }).then(
        function(response) {
          $mdDialog.show(
            $mdDialog.alert()
              .clickOutsideToClose(true)
              .textContent(`Lưu thông tin thành công`)
              .ok('Đóng')
            );
        },function(response) {
          window.location.reload();
        }
      );
    }
  }
)
