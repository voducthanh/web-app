var _1000_page_admin_news_list = angular.module( '_1000_page_admin_news_list', [] );

_1000_page_admin_news_list.controller('adminNewsListController',
  function($rootScope, $scope, $mdDialog, $http, $state){
    /* check auth */
    $scope.$emit('checkAuth');

    $scope.$emit('hideMenu', { message: true });

    /* Init data */
    $scope.dataNews = $rootScope.data.blog;

    $scope.changeActive = function(product) {
      $http({
        method: 'POST',
        url: `/update-vm/news/${product.id}/active`,
        data: {
          active: !product.active
        }
      }).then(
        function(response) {
        },function(response) {
          window.location.reload();
        }
      );
    }

    $scope.addNewNews = function() {
      $http({
        method: 'POST',
        url: '/update-vm/news/create'
      }).then(
        function(response) {
          $scope.dataNews.push(response.data.newNews);

          setTimeout(function() {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .textContent(`Tin tức mới đuợc tạo thành công với mã số: ${response.data.newNews.id}. Bấm vào chỉnh sửa để cập nhật thông tin`)
                .ok('Đóng')
            );
          }, 1000);

          $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        },function(response) {
          window.location.reload();
        }
      );
    }

    $scope.viewDetail = function(product) {
      $state.go('newsDetail', {id: product.nameCode});
    }
  }
);
