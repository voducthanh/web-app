var _1000_page_admin_tag = angular.module( '_1000_page_admin_tag', [] );

_1000_page_admin_tag.controller('adminTagController',
  function($rootScope, $scope, $mdDialog, $http, $state){
    /* check auth */
    $scope.$emit('checkAuth');

    $scope.$emit('hideMenu', { message: true });

    /* List color */
    $scope.colorList = ['red', 'green', 'blue', 'orange', 'cyan'];

    /* Init data */
    $scope.dataTag = $rootScope.data.tag;

    /* Change active map */
    $scope.changeActive = function (tag) {
      $http({
        method: 'POST',
        url: `/update-vm/tag/${tag.id}/active`,
        data: {
          active: !tag.active
        }
      }).then(
        function(response) {
        },function(response) {
          window.location.reload();
        }
      );
    }

    /* Change Color */
    $scope.changeColor = function(tag, color) {
      tag.color = color;
      $http({
        method: 'POST',
        url: `/update-vm/tag/save`,
        data: {
           tag: $scope.dataTag
        }
      });
    }

    /* Save map info */
    $scope.saveTag = function () {
      $http({
        method: 'POST',
        url: `/update-vm/tag/save`,
        data: {
           tag: $scope.dataTag
        }
      }).then(
        function(response) {
          $mdDialog.show(
            $mdDialog.alert()
              .clickOutsideToClose(true)
              .textContent(`Lưu thông tin thành công`)
              .ok('Đóng')
            );
        },function(response) {
          window.location.reload();
        }
      );
    }

    /* Add new Tag */
    $scope.addNewTag = function () {
      $http({
        method: 'POST',
        url: '/update-vm/tag/create'
      }).then(
        function(response) {
          $scope.dataTag.push(response.data.newTag);
          setTimeout(function() {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .textContent(`Thẻ mới đuợc tạo thành công với mã số: ${response.data.newTag.id}.`)
                .ok('Đóng')
            );
          }, 1000);

          $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        },function(response) {
          window.location.reload();
        }
      );
    }
  }
)