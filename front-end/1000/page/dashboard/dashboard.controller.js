var _1000_page_dashboard = angular.module( '_1000_page_dashboard', [] );

_1000_page_dashboard.controller('dashboardController', function($scope, $mdDialog){
  /* check auth */
  $scope.$emit('checkAuth');

  $scope.$emit('hideMenu', { message: true });
});
