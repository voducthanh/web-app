var _1000_page_product_detail = angular.module( '_1000_page_product_detail', [] );

_1000_page_product_detail.controller('productDetailController', ['$rootScope', '$scope', '$stateParams', 'fancyboxService', '$state',
  function($rootScope, $scope, $stateParams, fancyboxService, $state){
    $("html, body").animate({ scrollTop: "0" });

    var nameCodeProduct = $stateParams.id;
    for(index in $rootScope.data.article) {
      if ($rootScope.data.article[index].baseInfo.nameCode === nameCodeProduct) {
        $scope.data = $rootScope.data.article[index];
        break;
      }
    }

    $scope.filterListProduct = [];
    var dataFilter = {};
    for(index in $scope.data.filterDisplay) {
      for(indexSub in $rootScope.data.filter) {
        var temp = {};
        if ($scope.data.filterDisplay[index].id * 1 === $rootScope.data.filter[indexSub].id * 1) {
          temp.title = $rootScope.data.filter[indexSub].title;
          temp.icon = $rootScope.data.filter[indexSub].icon;
          temp.preTitle = $rootScope.data.filter[indexSub]['pre-title'];
          temp.posTitle = $rootScope.data.filter[indexSub]['pos-title'];

          for(indexList in $rootScope.data.filter[indexSub].list) {
            if ($scope.data.filterDisplay[index].value === $rootScope.data.filter[indexSub].list[indexList].id) {
              temp.value = $rootScope.data.filter[indexSub].list[indexList].value;
            }
          }

          $scope.filterListProduct.push(temp);
        }
      }
    }

    console.log($scope.filterListProduct);

    $scope.mapInfo = {};
    for(index in $rootScope.data.map) {
      if ($rootScope.data.map[index].id * 1 === $scope.data.baseInfo.mapBackground * 1) {
        $scope.mapInfo = $rootScope.data.map[index];
      }
    }

    /* General document */
    $scope.data.documentsList = [];
    for(index in $scope.data.documents) {
      $scope.data.documentsList.push($scope.data.documents[index]);
    }
    for(index in $rootScope.data.generalDocument) {
      $scope.data.documentsList.push($rootScope.data.generalDocument[index]);
    }

    /* Bread crumb */
    switch($scope.data.productType) {
      case 'home':
        $scope.listBreadcrumb = [
          {
            title: 'Dự án',
            link: '#'
          },
          {
            title: 'Nhà phố',
            link: '#'
          }
        ];
        break;
       default: brea;
    }

    /* FILTER */
    for(index in $scope.data.filterDisplay) {
      switch($scope.data.filterDisplay[index].key) {
        case 'area':
          $scope.data.filterDisplay[index].textHeading = 'Diện tích';
          break;

        case 'direction':
          $scope.data.filterDisplay[index].textHeading = 'Huớng nhà';
          break;

        case 'floor':
          $scope.data.filterDisplay[index].textHeading = 'Số tầng';
          break;

        case 'street':
          $scope.data.filterDisplay[index].textHeading = 'Đuờng truớc nhà';
          break;

        case 'bedRoom':
          $scope.data.filterDisplay[index].textHeading = 'Phòng ngủ';
          break;

        case 'bathRoom':
          $scope.data.filterDisplay[index].textHeading = 'Phòng tắm';
          break;

        case 'functionRoom':
          $scope.data.filterDisplay[index].textHeading = 'Phòng đa chức năng';
          break;

        default: break;
      }
    }

    for(index in $scope.data.filterInfo) {
      switch($scope.data.filterInfo[index].key) {
        case 'nameCode':
          $scope.data.filterInfo[index].textHeading = 'Mã số';
          break;

        case 'type':
          $scope.data.filterInfo[index].textHeading = 'Loại Dự án';
          break;

        case 'juridical':
          $scope.data.filterInfo[index].textHeading = 'Pháp lý';
          break;

        case 'progress':
          $scope.data.filterInfo[index].textHeading = 'Tiến độ';
          break;

        case 'status':
          $scope.data.filterInfo[index].textHeading = 'Trạng thái';
          break;

        case 'datePost':
          $scope.data.filterInfo[index].textHeading = 'Ngày đăng';
          break;

        default: break;
      }
    }

    /* Image */
    if ($scope.data.images.length === 0) {
      $scope.data.images = $rootScope.data.generalImage;
    }

    /* Document */

    $scope.downloadDocument = function () {
       window.open($scope.data.download);
    }
    $scope.configGallery = {
      'padding'     : 0,
      'transitionIn'    : 'none',
      'transitionOut'   : 'none',
      'type'              : 'image',
      'overlayOpacity': '0.8',
      'overlayColor': '#000'
    };

    $scope.openGallery = function(file) {
      if ('file-word-o' === file.icon) {
        window.open([
            'http://',
            document.domain,
            'vm.me' === document.domain ? ':3000' : '',
            file.src.replace('./', '/')
          ].join('')
        );
        return;
      }

      var gallery = [];
      for(index in $scope.data.documents) {
        if ('file-word-o' !== $scope.data.documents[index].icon) {
          gallery.push($scope.data.documents[index].src);
        }
      }
      fancyboxService.fancyboxPlus()(gallery, $scope.configGallery);
    }

    /* Related */
    $scope.productRelated = [];
    var rand;
    for(i = 0; i < 7; i++) {
      rand = Math.floor((Math.random() * $rootScope.data.article.length - 1) + 1);
      $scope.productRelated.push($rootScope.data.article[rand]);
    }

    $scope.navigateRelated = function (related) {
      $state.go('productDetail', {id: related.baseInfo.nameCode});
    }
  }
]);