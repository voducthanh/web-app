deferredBootstrapper.bootstrap({
  element: document.body,
  module: 'webApp',
  resolve: {
    APP_CONFIG: ['$http', function ($http) {
      return $http.get('/data-vm');
    }]
  }
});

angular.module('webApp', [
  'ngMaterial',

  /*  App */
  '_1000_app',

  /*  Page*/
  '_1000_page_home',
  '_1000_page_product',
  '_1000_page_product_detail',
  '_1000_page_news',
  '_1000_page_news_detail',

  /* Admin Page */
  '_1000_page_login',
  '_1000_page_dashboard',
  '_1000_page_admin_product_list',
  '_1000_page_admin_product_detail',
  '_1000_page_admin_news_list',
  '_1000_page_admin_map',
  '_1000_page_admin_tag',
  '_1000_page_admin_filter',
  '_1000_page_admin_image',
  '_1000_page_admin_document',

  /*  Module*/

  /*  Menu */
  '_1000_menuMain',
  '_1000_menuLeft',
  '_1000_menuRight',

  /*  Slide */
  '_1000_slideFull',

  /*  Product */
  '_1000_productList',
  '_1000_productItem',
  '_1000_productItemList',
  '_1000_productFilter',
  '_1000_productImage',

  /*  Introduce */
  '_1000_introduce',

  /*  Statistic */
  '_1000_statistic',

  /*  News */
  '_1000_newsList',
  '_1000_newsItem',
  '_1000_newsWidget',

  /*  Footer */
  '_1000_footer',

  /*  Breadcumb */
  '_1000_breadcrumb',

  /*  Pagination */
  '_1000_pagination',

  /*  Widget Container */
  '_1000_widget',

  /*  Category */
  '_1000_categoryWidget',

  /*  Keyword */
  '_1000_keywordWidget',

  /*  Admin */
  '_1000_adminMenu',

  /* Fancy box */
  'fancyboxplus',
])
  .config(function (APP_CONFIG) {
  })
  .run(function ($rootScope, $http, APP_CONFIG) {
    $rootScope.data = APP_CONFIG;
  });
