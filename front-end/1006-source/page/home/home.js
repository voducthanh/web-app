class homeController {
  constructor($rootScope, $scope, $state, $timeout) {
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$state = $state;
    this.$timeout = $timeout;
    this.$scope.productList = $rootScope.productList;

    /* Init data for product list */
    this.$scope.productList = this.$scope.productList.map(product => {
      product.item = product.id;
      product.focus = 0;
      return product;
    });


    $scope.naviagteManage = () => {
      this.focusAndNavigate();
    }
    // auto navigate to manage page
    // this.focusAndNavigate();

    /* Rotate image list */
    $scope.rotateImage = (_product) => {
      let checkWithoutImage = false;
      this.$scope.productList.map(product => {
        if (3 === product.item && false === product.active) {
          checkWithoutImage = true;
        }
      });

      if (3 === _product.item && !checkWithoutImage) {
        this.focusAndNavigate();
      }
    }

    /* Slide image by button */
    $scope.slideImage = (_direction) => {
      _direction = 'left' === _direction ? 1 : -1;

      this.$scope.productList = this.$scope.productList.map(bag => {
        bag.item = bag.item + _direction;
        bag.item = 6 === bag.item ? 1 : bag.item;
        bag.item = 0 === bag.item ? 5 : bag.item;
        return bag;
      });

      this.preloadImageThumbnail();
    }

    /* Get product name */
    $scope.getProductSelected = () => {
      let productName = '';
      this.$scope.productList.map(product => {
        if (3 === product.item) {
          productName = product.title;
        }
      })

      return productName;
    }

    /* Disble button select when product without info */
    $scope.disabledProduct = () => {
      let checkWithoutImage = false;
      this.$scope.productList.map(product => {
        if (3 === product.item && false === product.active) {
          checkWithoutImage = true;
        }
      });

      return checkWithoutImage;
    }

    /* Preload image thumbnail */
    this.preloadImageThumbnail();
  }

  preloadImageThumbnail() {
    this.$scope.productList.map(product => {
      if (3 === product.item) {
        this.$rootScope.preloadImage([`./1006/style/image/product/${product.code}/white/0.png`]);
      }
    });
  }

  focusAndNavigate() {
    this.$scope.productList = this.$scope.productList.map(product => {
      if (3 === product.item) {
        product.focus = 1;
        localStorage.setItem('product-selected', JSON.stringify(product));
      } else {
        product.focus = 2;
      }
      return product;
    });

    this.$timeout(() => {
      this.$state.go('manage')
    }, 300);
  }
}

angular
  .module('home', ['ui.router'])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      $stateProvider
        .state('home', {
          url: "/",
          template: '<home></home>'
        })
    }
  ])
  .directive('home', [() => {
    return {
      restrict: 'E',
      templateUrl: './1006/page/home/home.html',
      transclude: true,
      controller: homeController
    }
  }]);
