// import $ from "jquery";

const STEP_LIST = [
  {
    id: 1,
    status: 0,
    focus: true,
    title: '1. Bag Color',
    description: 'Use the color palette on the left to select the color of your bag.',
    active: './1006/style/image/asset/step-1-active.png',
    inactive: './1006/style/image/asset/step-1-active.png',
    value: ''
  },
  {
    id: 2,
    status: -1,
    focus: false,
    title: '2. Design',
    description: 'Scroll through the design options and select your favorite tea rose arrangement.',
    active: './1006/style/image/asset/step-2-active.png',
    inactive: './1006/style/image/asset/step-2.png',
    value: ''
  },
  {
    id: 3,
    status: -1,
    focus: false,
    title: '3. Your Tea Roses',
    description: 'Tap any tea rose on your bag and then select your favorite bloom.',
    active: './1006/style/image/asset/step-3-active.png',
    inactive: './1006/style/image/asset/step-3.png',
    value: ''
  }
]

class manageController {
  constructor($rootScope, $scope, $state, $http, $filter) {
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$state = $state;
    this.$http = $http;
    this.$filter = $filter;

    this.$scope.stateNavigateStyle = 0;
    this.$scope.showAboutPanel = false;
    this.$scope.showSendPanel = false;

    this.$scope.infoCheck = {
      purchase: false,
      ship: false
    }

    this.$scope.info = {
      customerFirstName: '',
      customerLastName: '',
      customerPhone: '',
      customerEmail: '',
      bagName: '',
      bagColor: '',
      bagStyle: '',
      bagListOfSelectRose: '',
      bagAttachement: '',
      infoType: '',
      infoAssociateId: '',
      infoShippingAddress: ''
    };


    this.initData();

    /* Get icon for step */
    this.$scope.getIconStep = (_step) => {
      return 1 === _step.status || true === _step.focus ? _step.active : _step.inactive;
    }

    /* Select color */
    this.$scope.selectColor = (_color) => {
      if (true === _color.hide) {
        return;
      }

      /* Remove all list flower in style */
      // this.$scope.styleFlower = [];

      this.$scope.productColor = this.$scope.productColor.map(color => {
        if (color.code === _color.code) {
          /* Set active for list */
          color.active = true;

          /* Set value for config */
          this.$scope.productSelected.config.color = _color;

          /* Change state for step */
          this.$scope.stepList.map(step => {
            /* Set completed for step 1*/
            step.status = 1 === step.id ? 1 : step.status;
            step.value = 1 === step.id ? _color.title : step.value;

            /* Set empty and activacble for step 2*/
            step.status = (2 === step.id && -1 === step.status) ? 0 : step.status;
          });
        } else {
          color.active = false;
        }

        return color;
      });
    }

    /* Select style */
    this.$scope.selectStyle = (_style) => {
      this.$scope.productStyle = this.$scope.productStyle.map(style => {
        if (style.code === _style.code) {
          /* Set active for list */
          style.active = true;

          /* Set value for config */
          this.$scope.productSelected.config.style = _style;

          /* Change state for step */
          this.$scope.stepList.map(step => {
            /* Set completed for step 1*/
            step.status = 2 === step.id ? 1 : step.status;
            step.value = 2 === step.id ? 'Style ' + style.code : step.value;


            /* Set empty and activacble for step 2*/
            step.status = 3 === step.id ? 0 : step.status;
          });

          /* Create list flower in style */
          this.$scope.styleFlower = style.flower;

          /* re check complete of selection flower */
          this.checkCompleteSelectFlower();
        } else {
          style.active = false;
        }

        return style;
      });
    }

    /* Change step */
    this.$scope.changeStep = (_step) => {
      /* Click into opening step -> return */
      if (true === _step.focus) {
        return;
      }

      switch (_step.id) {
        case 1:
          this.$scope.stepList = this.$scope.stepList.map(step => {
            step.focus = 1 === step.id;

            /* Remove style had config */
            // this.$scope.productSelected.config.style = '';

            /* Remove selected item in style list */
            // this.$scope.productStyle = this.$scope.productStyle.map(style => {
            //   style.active = false;
            //   return style;
            // });

            /* Change step 2 to not completed === 1 */
            // step.status = (2 === step.id || 3 === step.id) ? 0 : step.status;
            return step;
          });
          break;

        case 2:
          this.$scope.stepList = this.$scope.stepList.map(step => {
            /* Set focus into step 2 */
            step.focus = 2 === step.id;

            /* Set status 0 for step 2*/
            step.status = (2 === step.id && -1 === step.status) ? 0 : step.status;
            return step;
          });
          break;

        case 3:
          this.$scope.stepList = this.$scope.stepList.map(step => {
            /* Set focus into step 3 */
            step.focus = 3 === step.id;

            /* Set status 0 for step 3*/
            step.status = (3 === step.id && -1 === step.status) ? 0 : step.status;
            return step;
          });

          this.$rootScope.preloadImage(
            [
              './1006/style/image/petal/line/large-black.png',
              './1006/style/image/petal/line/large-white.png',
              './1006/style/image/petal/line/medium-black.png',
              './1006/style/image/petal/line/medium-white.png',
              './1006/style/image/petal/line/small-black.png',
              './1006/style/image/petal/line/small-white.png',

              './1006/style/image/petal/bag/large-1.png',
              './1006/style/image/petal/bag/large-2.png',
              './1006/style/image/petal/bag/large-3.png',
              './1006/style/image/petal/bag/large-4.png',
              './1006/style/image/petal/bag/medium-1.png',
              './1006/style/image/petal/bag/medium-2.png',
              './1006/style/image/petal/bag/medium-3.png',
              './1006/style/image/petal/bag/medium-4.png',
              './1006/style/image/petal/bag/medium-5.png',
              './1006/style/image/petal/bag/medium-6.png',
              './1006/style/image/petal/bag/medium-7.png',
              './1006/style/image/petal/bag/medium-8.png',
              './1006/style/image/petal/bag/small-1.png',
              './1006/style/image/petal/bag/small-2.png',
              './1006/style/image/petal/bag/small-3.png',
              './1006/style/image/petal/bag/small-4.png',

              './1006/style/image/petal/select/inactive/large-1.png',
              './1006/style/image/petal/select/inactive/large-2.png',
              './1006/style/image/petal/select/inactive/large-3.png',
              './1006/style/image/petal/select/inactive/large-4.png',
              './1006/style/image/petal/select/inactive/medium-1.png',
              './1006/style/image/petal/select/inactive/medium-2.png',
              './1006/style/image/petal/select/inactive/medium-3.png',
              './1006/style/image/petal/select/inactive/medium-4.png',
              './1006/style/image/petal/select/inactive/medium-5.png',
              './1006/style/image/petal/select/inactive/medium-6.png',
              './1006/style/image/petal/select/inactive/medium-7.png',
              './1006/style/image/petal/select/inactive/medium-8.png',
              './1006/style/image/petal/select/inactive/small-1.png',
              './1006/style/image/petal/select/inactive/small-2.png',
              './1006/style/image/petal/select/inactive/small-3.png',
              './1006/style/image/petal/select/inactive/small-4.png',
            ]
          );
          break;
      }
    }

    /* Get config bag */
    // this.$scope.getConfigProduct = () => {
    //   /* Not slect colro yet -> get white default */
    //   if ('' === this.$scope.productSelected.config.color) {
    //     return `./1006/style/image/product/${this.$scope.productSelected.code}/white/0.png`;
    //   }

    //   /* Had selected color && not select style yet -> get bag with color: no style */
    //   if ('' === this.$scope.productSelected.config.style) {
    //     return `./1006/style/image/product/${this.$scope.productSelected.code}/${this.$scope.productSelected.config.color.code}/0.png`;
    //   }

    //   /* Had select style */
    //   // return `./1006/style/image/product/${this.$scope.productSelected.code}/${this.$scope.productSelected.config.color.code}/${this.$scope.productSelected.config.style.code}.png`;
    //   return `./1006/style/image/product/${this.$scope.productSelected.code}/${this.$scope.productSelected.config.color.code}/0.png`;
    // }

    this.$scope.getConfigProductColor = (_color) => {
      return `./1006/style/image/product/${this.$scope.productSelected.code}/${_color}/0.png`;
      // return `./1006/style/image/product/${this.$scope.productSelected.code}/${this.$scope.productSelected.config.color.code}/${this.$scope.productSelected.config.style.code}.png`;
    }

    this.$scope.hideConfigProductColor = (_color) => {
      return _color !== this.$scope.productSelected.config.color.code;
    }

    /* Get value active tab left */
    this.$scope.getActiveTabLeft = (_stepIndex) => {
      let activeTab = false;
      this.$scope.stepList.map(step => {
        if (_stepIndex === step.id) {
          activeTab = true === step.focus;
        }
      });

      return activeTab;
    }

    /* Get tuhmbnail for style item */
    this.$scope.getThumbnailStyle = (_style) => {
      return '' === this.$scope.productSelected.config.color
        ? ''
        : `./1006/style/image/product/${this.$scope.productSelected.code}/${this.$scope.productSelected.config.color.code}/${_style.code}.png`;
    }

    /* Navigate Style */
    this.$scope.navigateStyle = (_navButton) => {
      switch (_navButton) {
        case 'top':
          this.$scope.stateNavigateStyle = 0;
          break;

        case 'down':
          this.$scope.stateNavigateStyle = 0 === this.$scope.stateNavigateStyle
            ? this.$scope.productStyle.length - 4
            : this.$scope.stateNavigateStyle;
          break;
      }
    }

    /* Disable button navigate style*/
    this.$scope.disabledNavigateStyle = (_navButton) => {
      switch (_navButton) {
        case 'top':
          return 0 === this.$scope.stateNavigateStyle;
          break;

        case 'down':
          return 0 !== this.$scope.stateNavigateStyle || this.$scope.productSelected.style.length <= 4;
          break;
      }
    }

    /* Set background flower on sidebar left */
    this.$scope.getFlowerBackground = (_flower) => {
      let flowerStyleSelected = this.$scope.styleFlower.filter(flower => 1 === flower.status || 2 === flower.status);

      //return 0 === flowerStyleSelected.length || _flower.type === flowerStyleSelected[0].type
      //? `background-image: url('./1006/style/image/petal/select/active/${_flower.type}-${_flower.title}.png')`
      //: `background-image: url('./1006/style/image/petal/select/active/${_flower.type}-${_flower.title}.png'); opacity: 0.45`
      return `background-image: url('./1006/style/image/petal/select/active/${_flower.type}-${_flower.title}.png')`;
    }
    this.$scope.disableFlower = (_flower) => {
      let flowerStyleSelected = this.$scope.styleFlower.filter(flower => 1 === flower.status || 2 === flower.status);

      return 0 === flowerStyleSelected.length || _flower.type === flowerStyleSelected[0].type;
    }

    /* Set style for flower */
    this.$scope.getFlowerStyle = (_flower) => {
      return `
        top: ${_flower.top}px;
        left: ${_flower.left}px;
        z-index: ${_flower.index || 0};
      `;
    }

    this.$scope.getFlowerItemStyle = (_flower, _type) => {
      let flowerUrl = '';
      if ('real' === _type) {
        let productFlowerSelected = this.$scope.productFlower.filter(flower => flower.id === _flower.value);
        flowerUrl = '' !== _flower.value
          ? `./1006/style/image/petal/bag/${productFlowerSelected[0].type}-${productFlowerSelected[0].title}.png`
          : '';
      } else {
        let colorOfLine = 'white';
        this.$scope.colorOfLine.map(color => {
          if (color.code === this.$scope.productSelected.config.color.code) {
            colorOfLine = color.line;
          }
        });

        flowerUrl = `./1006/style/image/petal/${_type}/${_flower.type}-${colorOfLine}.png`;
      }

      return `
        background-image: url('${flowerUrl}');
        transform: rotate(${_flower.rotate}deg) scale(${_flower.scale});
        -webkit-transform: rotate(${_flower.rotate}deg) scale(${_flower.scale});
        -moz-transform: rotate(${_flower.rotate}deg) scale(${_flower.scale});
      `;
    }

    this.$scope.hideFlowerItem = (_flower, _type) => {
      switch (_type) {
        case 'dot':
          return '' !== _flower.value
            ? true
            : 1 === _flower.status;
          break;

        case 'line':
          return '' !== _flower.value
            ? true
            : 0 === _flower.status || -1 === _flower.status;
          break;

        case 'real':
          return '' === _flower.value;
          break;
      }
    }

    /* Change flower config on the bag */
    this.$scope.flowerConfigChange = (_flower) => {
      let checkFocusTabFlower = false;
      this.$scope.stepList.map(step => {
        checkFocusTabFlower = 3 === step.id && true === step.focus;
      });

      if (false === checkFocusTabFlower) {
        return;
      }

      switch (_flower.status) {
        case -1:
        case 0:
          this.$scope.styleFlower = this.$scope.styleFlower.map(flower => {
            if (flower.id === _flower.id) {
              flower.status = '' === flower.value ? 1 : 2;
            } else {
              flower.status = 0;
            }
            return flower;
          });
          break;

        case 1:
        case 2:
          // this.$scope.styleFlower = this.$scope.styleFlower.map(flower => {
          //   flower.status = -1;
          //   return flower;
          // });
          break;
      }
    }

    /* select flower in sidebar left into config */
    this.$scope.flowerSelect = (_flower) => {
      this.$scope.styleFlower = this.$scope.styleFlower.map(flower => {
        if ((1 === flower.status || 2 === flower.status) && _flower.type === flower.type) {
          flower.value = _flower.id;
        }

        return flower;
      });

      this.$scope.productSelected.config.tearose = this.$scope.styleFlower;

      this.checkCompleteSelectFlower();
    }

    /* Navigate to Home */
    this.$scope.navigateToHome = () => {
      if (!this.$scope.showAboutPanel) {
        let confirmBackHome = confirm("Note: By returning to the bag silhouette selection screen, you will lose your current selection of tea roses.");
        if (true === confirmBackHome) {
          window.location = '/';
        }
      } else {
        return false;
      }
    }

    /* Toggle about panel */
    this.$scope.toggleAboutPanel = () => {
      this.$scope.showAboutPanel = !this.$scope.showAboutPanel;
    }

    /* Preload color bag */
    this.preloadImageColor();

    /* Disable Button Save */
    this.$scope.disableSave = () => {
      let disableButtonSave = this.$scope.stepList.filter(step => step.status === 1);
      return disableButtonSave.length !== 3;
    }

    this.$scope.confirmationPopup = document.querySelector('.confirmation-popup');
    this.$scope.loadingPopup = document.querySelector('.loading-popup');
    this.$scope.sendContent = document.querySelector('.send-content');

    /* Open /close panel */
    this.$scope.toogleSendPanel = (_state = false) => {
      this.$scope.showSendPanel = _state;
      this.$scope.loadingPopup.style.display = 'none';
      if (_state) {
        this.$scope.confirmationPopup.style.display = 'none';
        this.$scope.sendContent.style.display = 'block';
      } else {
        this.$scope.confirmationPopup.style.display = 'block';
        this.$scope.sendContent.style.display = 'none';
      }

      this.$scope.stepList = this.$scope.stepList.map(step => {
        if (true === _state) {
          step.focus = false;
          step.status = 1;
        } else {
          if (1 === step.id) {
            step.focus = true;
          }
        }
        return step;
      });

      window.html2canvas($("#config-product"), {
        onrendered: (canvas) => {
          this.$scope.info.bagAttachement = canvas.toDataURL();
        }
      });
    }
    // auto open form contact
    // this.$scope.toogleSendPanel(true);

    this.$scope.infoAssociateIdFocus = false;
    this.$scope.customerFirstNameFocus = false;
    this.$scope.customerLastNameFocus = false;
    this.$scope.customerEmailFocus = false;
    this.$scope.customerPhoneFocus = false;
    /* Disabled button submit */
    this.$scope.disableSubmit = () => {
      return '' === this.$scope.info.infoAssociateId
        || '' === this.$scope.info.customerFirstName
        || '' === this.$scope.info.customerLastName
    }

    /* Submit Save & Send */
    this.$scope.submitForm = () => {
      this.$scope.info.customerPhone = this.$scope.info.customerPhone + '';
      this.$scope.info.bagName = this.$scope.productSelected.title;
      this.$scope.info.bagColor = this.$scope.productSelected.config.color.title.replace('-', ' ').toUpperCase();
      this.$scope.info.bagStyle = `STYLE ${this.$scope.productSelected.config.style.code}`;
      this.$scope.info.bagListOfSelectRose = this.$scope.productSelected.config.tearose.map(flower => flower.value).join(',');
      if (this.$scope.info.customerPhone) {
        this.$scope.info.customerPhone = this.$scope.info.customerPhone;
      }
      document.body.style.cursor = 'progress';
      this.$scope.loadingPopup.style.display = 'block';
      // var URL = 'http://localhost:8000/api/bags';
      var URL = '/api/bags';
      console.log(this.$scope.info);
      this.$http.post(
        URL,
        this.$scope.info,
        {}
      ).then((res) => {
        console.log(res);
        document.body.style.cursor = 'default';
        this.$scope.confirmationPopup.style.display = 'block';
        this.$scope.sendContent.style.display = 'none';
        this.$scope.loadingPopup.style.display = 'none';
      }, () => {
        document.body.style.cursor = 'default';
        this.$scope.loadingPopup.style.display = 'none';
        alert('An error has occurred, please try again');
      });

      //bagAttachement: ''
    }

    /* togglePurchase */
    this.$scope.togglePurchase = () => {
      this.$scope.infoCheck.purchase = !this.$scope.infoCheck.purchase;
      if (false === this.$scope.infoCheck.purchase) {
        this.$scope.infoCheck.ship = false;
        this.$scope.info.infoType = ''
      } else {
        this.$scope.info.infoType = 'purchase'
      }
    }

    /* toggleShip */
    this.$scope.toggleShip = () => {
      this.$scope.infoCheck.ship = !this.$scope.infoCheck.ship;
      if (false === this.$scope.infoCheck.ship) {
        this.$scope.info.infoType = 'purchase'
      } else {
        this.$scope.info.infoType = 'ship'
      }
    }

    /* disabledButtonShip */
    this.$scope.disabledCheckboxShip = () => {
      return !this.$scope.infoCheck.purchase;
    }
  }

  initData() {
    /* Color of line */
    this.$scope.colorOfLine = this.$rootScope.colorOfLine;

    /* Get step list info */
    this.$scope.stepList = STEP_LIST;

    /* Get product selected from prev step */
    let _productSelected = localStorage.getItem('product-selected');
    if (null === _productSelected || '' === _productSelected) {
      this.$state.go('home');
    }

    this.$scope.productSelected = JSON.parse(_productSelected);

    /* Confir select color / style / tearose */
    this.$scope.productSelected.config = {
      color: '',
      style: '',
      tearose: []
    };

    /* Init list color of product */
    this.$scope.productColor = this.$rootScope.colorList.filter(
      color =>
        !('white' === color.code || 0 > this.$scope.productSelected.color.indexOf(color.code))
    );

    /* add empty colro pallet */
    for (let i = 0; i < 16 - this.$scope.productSelected.color.length; i++) {
      this.$scope.productColor.push({
        id: 1,
        code: 'white',
        title: 'White',
        color: '#FFFFFF',
        hide: true
      });
    }

    /* Init list style of product */
    this.$scope.productStyle = this.$scope.productSelected.style;

    /* Init list flower of side bar left */
    this.$scope.productFlower = this.$rootScope.flowerList;

    /* init style flower */
    this.$scope.styleFlower = [];

    console.log('this.$scope.colorList', this.$scope.colorList);
    console.log('this.$scope.productSelected', this.$scope.productSelected);
    console.log('this.$scope.productColor', this.$scope.productColor);
    console.log('this.$scope.productStyle', this.$scope.productStyle);
    console.log('this.$scope.productFlower', this.$scope.productFlower);
  }

  /* preload color bag */
  preloadImageColor() {
    this.$rootScope.preloadImage(
      this.$scope.productSelected.color.map(color => `./1006/style/image/product/${this.$scope.productSelected.code}/${color}/0.png`)
    );

    this.$rootScope.preloadImage(
      [
        './1006/style/image/petal/dot/large-black.png',
        './1006/style/image/petal/dot/large-white.png',
        './1006/style/image/petal/dot/medium-black.png',
        './1006/style/image/petal/dot/medium-white.png',
        './1006/style/image/petal/dot/small-black.png',
        './1006/style/image/petal/dot/small-white.png',
      ]
    );
  }

  /* Check complete step 3 */
  checkCompleteSelectFlower() {
    let checkCompletedStep3 = true;
    this.$scope.styleFlower.map(flower => {
      if ('' === flower.value) {
        checkCompletedStep3 = false;
      }
    });

    this.$scope.stepList = this.$scope.stepList.map(step => {
      if (3 === step.id) {
        step.status = true === checkCompletedStep3 ? 1 : 0;
      }

      return step;
    })
  }
}
angular
  .module('manage', ['ui.router'])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      $stateProvider
        .state('manage', {
          url: "/manage",
          template: '<manage></manage>'
        })
    }
  ])
  .directive('manage', [() => {
    return {
      restrict: 'E',
      templateUrl: './1006/page/manage/manage.html',
      transclude: true,
      controller: manageController
    }
  }]);
