class headerController { }

angular
  .module('header', [])
  .directive('header', () => {
    return {
      restrict: 'E',
      templateUrl: './1006/module/header/header.html',
      transclude: true,
      controller: headerController
    }
  })
