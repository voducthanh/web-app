/* DEFINED COLOR OF LINE FLOWER */
const COLOR_OF_LINE = [
  {
    id: 1,
    code: 'white',
    line: 'black'
  },
  {
    id: 2,
    code: 'black',
    line: 'white'
  },
  {
    id: 3,
    code: 'chalk',
    line: 'black'
  },
  {
    id: 4,
    code: 'bordeaux',
    line: 'white'
  },
  {
    id: 5,
    code: 'petal',
    line: 'black'
  },
  {
    /* With out define color */
    id: 6,
    code: 'neon-pink',
    line: 'black'
  },
  {
    id: 7,
    code: 'beechwood',
    line: 'white'
  },
  {
    id: 8,
    code: 'saddle',
    line: 'white'
  },
  {
    id: 9,
    code: 'red',
    line: 'white'
  },
  {
    id: 10,
    code: 'dark-denim',
    line: 'white'
  },
  {
    /* With out define color */
    id: 11,
    code: 'heather-grey',
    line: 'black'
  },
  {
    id: 12,
    code: 'flax',
    line: 'black'
  },
  {
    /* With out define color */
    id: 13,
    code: 'olive',
    line: 'white'
  },
  {
    id: 14,
    code: 'peony',
    line: 'white'
  },
  {
    id: 15,
    code: 'cornflower',
    line: 'black'
  }
];

/* DEFINED GLOBAL VARIABLE STORAGE DATA */
const PRODUCT_LIST = [
  {
    id: 1,
    active: true,
    code: 'clutch',
    title: 'CLUTCH',
    thumbnail: './1006/style/image/product/clutch/thumbnail/0.png',
    thumbnailOpacity: './1006/style/image/product/clutch/thumbnail/1.png',
    image: './1006/style/image/',
    color: ['black', 'bordeaux', 'neon-pink', 'red', 'heather-grey', 'olive'],
    style: [
      {
        id: 1,
        code: 1,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'medium',
            top: 164,
            left: 134,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'small',
            top: 193,
            left: 196,
            rotate: 0,
            value: '',
            index: 2
          }
        ]
      },
      {
        id: 2,
        code: 2,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'medium',
            top: 180,
            left: 349,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'small',
            top: 162,
            left: 298,
            rotate: 0,
            value: '',
            index: 2
          }
        ]
      }
    ]
  },
  {
    id: 2,
    active: true,
    code: 'dinky-24',
    title: 'DINKY 24',
    thumbnail: './1006/style/image/product/dinky-24/thumbnail/0.png',
    thumbnailOpacity: './1006/style/image/product/dinky-24/thumbnail/1.png',
    image: './1006/style/image/',
    color: ['black', 'chalk', 'bordeaux'],
    style: [
      {
        id: 1,
        code: 1,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 322,
            left: 194,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 2,
            status: -1,
            type: 'medium',
            top: 353,
            left: 129,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 3,
            status: -1,
            type: 'small',
            top: 368,
            left: 273,
            rotate: 0,
            value: '',
            index: 3
          }
        ]
      },
      {
        id: 2,
        code: 2,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 368,
            left: 69,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'medium',
            top: 383,
            left: 321,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'small',
            top: 415,
            left: 145,
            rotate: 0,
            value: '',
            index: 3
          }
        ]
      },
      {
        id: 3,
        code: 3,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 297,
            left: 256,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 373,
            left: 277,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 324,
            left: 193,
            rotate: 0,
            value: '',
            index: 3
          },
          {
            id: 4,
            status: -1,
            type: 'small',
            top: 349,
            left: 98,
            rotate: 0,
            value: '',
            index: 4
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 376,
            left: 152,
            rotate: 0,
            value: '',
            index: 5
          }
        ]
      },
      {
        id: 4,
        code: 4,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 372,
            left: 129,
            rotate: 0,
            value: '',
            index: 5
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 309,
            left: 214,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 370,
            left: 269,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 4,
            status: -1,
            type: 'small',
            top: 369,
            left: 85,
            rotate: 0,
            value: '',
            index: 3
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 377,
            left: 339,
            rotate: 0,
            value: '',
            index: 4
          }
        ]
      },
      {
        id: 5,
        code: 5,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 346,
            left: 60,
            rotate: 0,
            value: '',
            index: 4
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 352,
            left: 134,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'large',
            top: 392,
            left: 296,
            rotate: 0,
            value: '',
            index: 5
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 300,
            left: 107,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 5,
            status: -1,
            type: 'medium',
            top: 338,
            left: 338,
            rotate: 0,
            value: '',
            index: 7
          },
          {
            id: 6,
            status: -1,
            type: 'small',
            top: 424,
            left: 88,
            rotate: 0,
            value: '',
            index: 3
          },
          {
            id: 7,
            status: -1,
            type: 'small',
            top: 402,
            left: 244,
            rotate: 0,
            value: '',
            index: 6
          }
        ]
      },
      {
        id: 6,
        code: 6,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 303,
            left: 111,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 388,
            left: 136,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'large',
            top: 349,
            left: 217,
            rotate: 0,
            value: '',
            index: 6
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 338,
            left: 65,
            rotate: 0,
            value: '',
            index: 3
          },
          {
            id: 5,
            status: -1,
            type: 'medium',
            top: 386,
            left: 293,
            rotate: 0,
            value: '',
            index: 5
          },
          {
            id: 6,
            status: -1,
            type: 'small',
            top: 272,
            left: 75,
            rotate: 0,
            value: '',
            index: 4
          },
          {
            id: 7,
            status: -1,
            type: 'small',
            top: 371,
            left: 355,
            rotate: 0,
            value: '',
            index: 7
          }
        ]
      }
    ]
  },
  {
    id: 3,
    active: true,
    code: 'dinky',
    title: 'DINKY',
    thumbnail: './1006/style/image/product/dinky/thumbnail/0.png',
    thumbnailOpacity: './1006/style/image/product/dinky/thumbnail/1.png',
    image: './1006/style/image/',
    color: ['black', 'chalk', 'bordeaux', 'petal', 'beechwood', 'saddle', 'red', 'dark-denim', 'flax', 'peony', 'cornflower'],
    style: [
      {
        id: 1,
        code: 1,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 322,
            left: 194,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 2,
            status: -1,
            type: 'medium',
            top: 353,
            left: 129,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 3,
            status: -1,
            type: 'small',
            top: 368,
            left: 272,
            rotate: 0,
            value: '',
            index: 3
          }
        ]
      },
      {
        id: 2,
        code: 2,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 368,
            left: 59,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'medium',
            top: 383,
            left: 321,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'small',
            top: 415,
            left: 135,
            rotate: 0,
            value: '',
            index: 3
          }
        ]
      },
      {
        id: 3,
        code: 3,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 291,
            left: 266,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 373,
            left: 277,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 324,
            left: 198,
            rotate: 0,
            value: '',
            index: 3
          },
          {
            id: 4,
            status: -1,
            type: 'small',
            top: 372,
            left: 152,
            rotate: 0,
            value: '',
            index: 4
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 354,
            left: 101,
            rotate: 0,
            value: '',
            index: 5
          }
        ]
      },
      {
        id: 4,
        code: 4,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 388,
            left: 130,
            rotate: 0,
            value: '',
            index: 5
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 330,
            left: 221,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 389,
            left: 287,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 4,
            status: -1,
            type: 'small',
            top: 382,
            left: 77,
            rotate: 0,
            value: '',
            index: 4
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 392,
            left: 355,
            rotate: 0,
            value: '',
            index: 3
          }
        ]
      },
      {
        id: 5,
        code: 5,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 326,
            left: 44,
            rotate: 0,
            value: '',
            index: 4
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 338,
            left: 127,
            rotate: 0,
            value: '',
            index: 3
          },
          {
            id: 3,
            status: -1,
            type: 'large',
            top: 373,
            left: 300,
            rotate: 0,
            value: '',
            index: 5
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 285,
            left: 97,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 5,
            status: -1,
            type: 'medium',
            top: 322,
            left: 357,
            rotate: 0,
            value: '',
            index: 6
          },
          {
            id: 6,
            status: -1,
            type: 'small',
            top: 411,
            left: 71,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 7,
            status: -1,
            type: 'small',
            top: 393,
            left: 246,
            rotate: 0,
            value: '',
            index: 7
          }
        ]
      },
      {
        id: 6,
        code: 6,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 293,
            left: 101,
            rotate: 0,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 379,
            left: 128,
            rotate: 0,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'large',
            top: 337,
            left: 205,
            rotate: 0,
            value: '',
            index: 6
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 348,
            left: 43,
            rotate: 0,
            value: '',
            index: 3
          },
          {
            id: 5,
            status: -1,
            type: 'medium',
            top: 386,
            left: 285,
            rotate: 0,
            value: '',
            index: 5
          },
          {
            id: 6,
            status: -1,
            type: 'small',
            top: 275,
            left: 57,
            rotate: 0,
            value: '',
            index: 4
          },
          {
            id: 7,
            status: -1,
            type: 'small',
            top: 371,
            left: 355,
            rotate: 0,
            value: '',
            index: 7
          }
        ]
      }
    ]
  },
  {
    id: 4,
    active: true,
    code: 'saddle-bag-23',
    title: 'SADDLE BAG 23',
    thumbnail: './1006/style/image/product/saddle-bag-23/thumbnail/0.png',
    thumbnailOpacity: './1006/style/image/product/saddle-bag-23/thumbnail/1.png',
    image: './1006/style/image/',
    color: ['black', 'chalk', 'bordeaux', 'saddle', 'dark-denim', 'heather-grey', 'flax', 'olive', 'peony', 'cornflower'],
    style: [
      {
        id: 1,
        code: 1,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 335,
            left: 208,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'medium',
            top: 328,
            left: 149,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'small',
            top: 324,
            left: 290,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 3
          }
        ]
      },
      {
        id: 2,
        code: 2,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 286,
            left: 308,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 3
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 334,
            left: 257,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 353,
            left: 197,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 1
          }
        ]
      },
      {
        id: 3,
        code: 3,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 323,
            left: 292,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 5
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 284,
            left: 236,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 4
          },
          {
            id: 3,
            status: -1,
            type: 'large',
            top: 342,
            left: 187,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 3
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 331,
            left: 124,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 1
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 302,
            left: 156,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 2
          }
        ]
      },
      {
        id: 4,
        code: 4,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 289,
            left: 312,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 5
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 323,
            left: 110,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 3
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 364,
            left: 176,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 2
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 345,
            left: 278,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 4
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 295,
            left: 109,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 1
          }
        ]
      },
      {
        id: 5,
        code: 5,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 336,
            left: 114,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 317,
            left: 312,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 5
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 289,
            left: 91,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 2
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 368,
            left: 268,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 6
          },
          {
            id: 5,
            status: -1,
            type: 'medium',
            top: 253,
            left: 338,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 7
          },
          {
            id: 6,
            status: -1,
            type: 'small',
            top: 383,
            left: 186,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 4
          },
          {
            id: 7,
            status: -1,
            type: 'small',
            top: 254,
            left: 96,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 3
          }
        ]
      },
      {
        id: 6,
        code: 6,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 313,
            left: 123,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 310,
            left: 200,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 4
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 357,
            left: 253,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 6
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 262,
            left: 101,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 2
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 378,
            left: 190,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 5
          },
          {
            id: 6,
            status: -1,
            type: 'small',
            top: 338,
            left: 317,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 7
          },
          {
            id: 7,
            status: -1,
            type: 'small',
            top: 221,
            left: 114,
            rotate: 0,
            scale: 0.85,
            value: '',
            index: 3
          }
        ]
      }
    ]
  },
  {
    id: 5,
    active: true,
    code: 'saddle-bag',
    title: 'SADDLE BAG',
    thumbnail: './1006/style/image/product/saddle-bag/thumbnail/0.png',
    thumbnailOpacity: './1006/style/image/product/saddle-bag/thumbnail/1.png',
    image: './1006/style/image/',
    color: ['black', 'heather-grey'],
    style: [
      {
        id: 1,
        code: 1,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 342,
            left: 212,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'medium',
            top: 337,
            left: 164,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'small',
            top: 337,
            left: 288,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 3
          }
        ]
      },
      {
        id: 2,
        code: 2,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 294,
            left: 311,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 3
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 336,
            left: 267,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 2
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 353,
            left: 215,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 1
          }
        ]
      },
      {
        id: 3,
        code: 3,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 336,
            left: 294,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 5
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 300,
            left: 238,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 4
          },
          {
            id: 3,
            status: -1,
            type: 'large',
            top: 352,
            left: 194,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 3
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 342,
            left: 120,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 1
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 322,
            left: 166,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 2
          }
        ]
      },
      {
        id: 4,
        code: 4,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 295,
            left: 318,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 5
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 317,
            left: 108,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 3
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 360,
            left: 168,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 2
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 349,
            left: 289,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 4
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 289,
            left: 106,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 1
          }
        ]
      },
      {
        id: 5,
        code: 5,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 339,
            left: 116,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 324,
            left: 309,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 5
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 299,
            left: 97,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 2
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 368,
            left: 268,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 6
          },
          {
            id: 5,
            status: -1,
            type: 'medium',
            top: 270,
            left: 332,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 7
          },
          {
            id: 6,
            status: -1,
            type: 'small',
            top: 383,
            left: 186,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 4
          },
          {
            id: 7,
            status: -1,
            type: 'small',
            top: 268,
            left: 101,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 3
          }
        ]
      },
      {
        id: 6,
        code: 6,
        flower: [
          {
            id: 1,
            status: -1,
            type: 'large',
            top: 334,
            left: 123,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 1
          },
          {
            id: 2,
            status: -1,
            type: 'large',
            top: 327,
            left: 206,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 4
          },
          {
            id: 3,
            status: -1,
            type: 'medium',
            top: 368,
            left: 261,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 6
          },
          {
            id: 4,
            status: -1,
            type: 'medium',
            top: 295,
            left: 97,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 2
          },
          {
            id: 5,
            status: -1,
            type: 'small',
            top: 379,
            left: 190,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 5
          },
          {
            id: 6,
            status: -1,
            type: 'small',
            top: 352,
            left: 317,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 7
          },
          {
            id: 7,
            status: -1,
            type: 'small',
            top: 262,
            left: 110,
            rotate: 0,
            scale: 0.75,
            value: '',
            index: 3
          }
        ]
      }
    ]
  }
];

/**
 * DEFINE COLOR LIST
 * @id:
 * @code: use in bag color list
 * title: use to show color name
 * color: use to show color pattern
 *
 */

const COLOR_LIST = [
  {
    id: 1,
    code: 'white',
    title: 'White',
    color: '#FFFFFF'
  },
  {
    id: 2,
    code: 'black',
    title: 'Black',
    color: '#000000'
  },
  {
    id: 3,
    code: 'chalk',
    title: 'Chalk',
    color: '#EEE6D9'
  },
  {
    id: 4,
    code: 'bordeaux',
    title: 'Bordeaux',
    color: '#4E1420',
  },
  {
    id: 5,
    code: 'petal',
    title: 'Petal',
    color: '#FDBACB'
  },
  {
    id: 6,
    code: 'neon-pink',
    title: 'Neon Pink',
    color: '#FF7EAE'
  },
  {
    id: 7,
    code: 'beechwood',
    title: 'Beechwood',
    color: '#C5977D'
  },
  {
    id: 8,
    code: 'saddle',
    title: '1941 Saddle',
    color: '#926E3A'
  },
  {
    id: 9,
    code: 'red',
    title: 'Red',
    color: '#a4162f'
  },
  {
    id: 10,
    code: 'dark-denim',
    title: 'Dark Denim',
    color: '#004C76'
  },
  {
    id: 11,
    code: 'heather-grey',
    title: 'Heather Grey',
    color: '#BAAFAD'
  },
  {
    id: 12,
    code: 'flax',
    title: 'Flax',
    color: '#C3A63D'
  },
  {
    id: 13,
    code: 'olive',
    title: 'Olive',
    color: '#4A4D31'
  },
  {
    id: 14,
    code: 'peony',
    title: 'Peony',
    color: '#AD686C'
  },
  {
    id: 15,
    code: 'cornflower',
    title: 'Cornflower',
    color: '#6f90ac'
  },
];

/* Defined list tearose */
const FLOWER_LIST = [
  {
    id: 1,
    title: 1,
    type: 'large',
    active: true
  },
  {
    id: 2,
    title: 2,
    type: 'large',
    active: true
  },
  {
    id: 3,
    title: 3,
    type: 'large',
    active: true
  },
  {
    id: 4,
    title: 4,
    type: 'large',
    active: true
  },
  {
    id: 5,
    title: 1,
    type: 'medium',
    active: true
  },
  {
    id: 6,
    title: 2,
    type: 'medium',
    active: true
  },
  {
    id: 7,
    title: 3,
    type: 'medium',
    active: true
  },
  {
    id: 8,
    title: 4,
    type: 'medium',
    active: true
  },
  {
    id: 9,
    title: 5,
    type: 'medium',
    active: true
  },
  {
    id: 10,
    title: 6,
    type: 'medium',
    active: true
  },
  {
    id: 11,
    title: 7,
    type: 'medium',
    active: true
  },
  {
    id: 12,
    title: 8,
    type: 'medium',
    active: true
  },
  {
    id: 13,
    title: 1,
    type: 'small',
    active: true
  },
  {
    id: 14,
    title: 2,
    type: 'small',
    active: true
  },
  {
    id: 15,
    title: 3,
    type: 'small',
    active: true
  },
  {
    id: 16,
    title: 4,
    type: 'small',
    active: true
  }
];

class appController {
  constructor($rootScope) {
    $rootScope.productList = PRODUCT_LIST;
    $rootScope.colorList = COLOR_LIST;
    $rootScope.flowerList = FLOWER_LIST;
    $rootScope.colorOfLine = COLOR_OF_LINE;

    document.addEventListener("touchstart", function () { }, true);
    document.ontouchmove = (event) => {
      event.preventDefault();
    }

    $rootScope.listImagePreload = [];
    $rootScope.preloadImage = (listImage) => {
      listImage.map(image => {
        if ($rootScope.listImagePreload.indexOf(image) < 0) {
          $rootScope.listImagePreload.push(image);
          let img = new Image();
          img.src = image;
        }
      });
    }
  }
}

angular
  .module('app', ['ui.router'])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      /*  404 */
      $urlRouterProvider.otherwise("/");
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: true
      });

      $locationProvider.hashPrefix('!');
    }
  ])

  .directive('app', () => {
    return {
      restrict: 'E',
      templateUrl: './1006/module/app/app.html',
      transclude: true,
      controller: appController
    }
  })

  .filter('unitTimeConverter', () => {
    return (unixTimeStamp) => {
      var newDate = new Date(unixTimeStamp * 1000);
      return [
        newDate.getDate(),
        '/',
        newDate.getMonth() + 1,
        '/',
        newDate.getFullYear()
      ].join("");
    }
  })

  .filter('unitTimeConverterToHour', () => {
    return (unixTimeStamp) => {
      var newDate = new Date(unixTimeStamp * 1000);
      return [
        newDate.getHours(),
        ':',
        newDate.getMinutes()
      ].join("");
    }
  })

  .directive('phoneInput', function ($filter, $browser) {
    return {
      require: 'ngModel',
      link: function ($scope, $element, $attrs, ngModelCtrl) {
        var listener = function () {
          var value = $element.val().replace(/[^0-9]/g, '');
          $element.val($filter('tel')(value, false));
        };

        // This runs when we update the text field
        ngModelCtrl.$parsers.push(function (viewValue) {
          return viewValue.replace(/[^0-9]/g, '').slice(0, 10);
        });

        // This runs when the model gets updated on the scope directly and keeps our view in sync
        ngModelCtrl.$render = function () {
          $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
        };

        $element.bind('change', listener);
        $element.bind('keydown', function (event) {
          var key = event.keyCode;
          // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
          // This lets us support copy and paste too
          if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
            return;
          }
          $browser.defer(listener); // Have to do this or changes don't get picked up properly
        });

        $element.bind('paste cut', function () {
          $browser.defer(listener);
        });
      }

    };
  })

  .filter('tel', function () {
    return function (tel) {
      if (!tel) { return ''; }

      var value = tel.toString().trim().replace(/^\+/, '');

      if (value.match(/[^0-9]/)) {
        return tel;
      }

      var country, city, number;

      switch (value.length) {
        case 1:
        case 2:
        case 3:
          city = value;
          break;

        default:
          city = value.slice(0, 3);
          number = value.slice(3);
      }

      if (number) {
        if (number.length > 3) {
          number = number.slice(0, 3) + '-' + number.slice(3, 7);
        }
        else {
          number = number;
        }

        return (city + "-" + number).trim();
      }
      else {
        return city;
      }

    };
  })
