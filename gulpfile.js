'use strict';

var
  gulp = require('gulp'),
  jade = require('gulp-jade'),
  sass = require('gulp-sass'),
  useref = require('gulp-useref'),
  browserSync = require('browser-sync').create(),
  reload = browserSync.reload,
  uglify = require('gulp-uglify'),
  gulpIf = require('gulp-if'),
  runSequence = require('run-sequence'),
  copy = require('gulp-contrib-copy'),
  data = require('gulp-data'),
  del = require('del'),
  concat = require('gulp-concat'),
  minifyCSS = require('gulp-minify-css'),
  autoprefixer = require('gulp-autoprefixer'),
  ngAnnotate = require('gulp-ng-annotate'),
  babel = require('gulp-babel'),
  nodemon = require('gulp-nodemon'),
  eslint = require('gulp-eslint');


/*===================================== 1006 ======================================*/
/*  GULP SASS */
/*  Vendor */
var listScssVendor_1006 = [
  './front-end/1006-source/style/scss/reset.scss',
];

gulp.task('sass-vendor_1006', function () {
  return gulp.src(listScssVendor_1006)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
    .pipe(concat('vendor.min.css'))
    .pipe(gulp.dest('./front-end/1006/style/css/'))
});

/*  Style */
var listScssStyle_1006 = [
  './front-end/1006-source/style/scss/layout.scss',
  './front-end/1006-source/style/scss/component.scss',
  './front-end/1006-source/style/scss/typography.scss',
  './front-end/1006-source/page/**/*.scss',
  './front-end/1006-source/module/**/*.scss'
];
gulp.task('sass-style_1006', function () {
  return gulp.src(listScssStyle_1006)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('./front-end/1006/style/css/'))
    .pipe(reload({ stream: true }));
});

/*  GULP JS */
/*  Vendor */
var listJsVendor_1006 = [
  './node_modules/jquery/dist/jquery.min.js',
  './front-end/1006-source/style/js/html2canvas.js',
  './node_modules/angular/angular.min.js',
  './node_modules/angular-ui-router/release/angular-ui-router.min.js',
];

gulp.task('js-vendor_1006', function () {
  return gulp.src(listJsVendor_1006)
    .pipe(ngAnnotate())
    .pipe(eslint({
      rules: {
        'my-custom-rule': 1,
        'strict': 2
      },
      globals: [
        'jQuery',
        '$'
      ],
      envs: [
        'browser'
      ]
    }))
    .pipe(uglify())
    .pipe(concat('vendor.min.js'))
    .pipe(gulp.dest('./front-end/1006/style/js/'))
});

/*  Script */
var listJsScript_1006 = [
  './front-end/1006-source/style/js/angular-application.js',
  './front-end/1006-source/page/**/*.js',
  './front-end/1006-source/module/**/*.js',
];

gulp.task('js-script_1006', function () {
  return gulp.src(listJsScript_1006)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(ngAnnotate())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./front-end/1006/style/js/'))
    .pipe(reload({ stream: true }));
});

gulp.task('js-script-minify_1006', function () {
  return gulp.src(listJsScript_1006)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./front-end/1006/style/js/'))
    .pipe(reload({ stream: true }));
});

/*  GULP JADE */
gulp.task('jade_1006', function () {
  var locals = {};
  var j = jade({
    locals: locals,
    pretty: false
  }).on('error', function (err) {
    console.log(err);
    j.end();
  });

  return gulp.src('./front-end/1006-source/**/*.jade')
    .pipe(j)
    .pipe(gulp.dest('./front-end/1006'))
    .pipe(reload({ stream: true }));
}
);

/*  GULP COPY FILE */
var listAsset_1006 = [
  './front-end/1006-source/**/*.png',
  './front-end/1006-source/**/*.jpg',
  './front-end/1006-source/**/*.gif',
  './front-end/1006-source/**/*.svg',
  './front-end/1006-source/**/*.eot',
  './front-end/1006-source/**/*.ttf',
  './front-end/1006-source/**/*.woff',
  './front-end/1006-source/**/*.woff2',
  './front-end/1006-source/**/*.otf'
];

gulp.task('copy-asset_1006', function () {
  gulp.src(listAsset_1006)
    .pipe(gulp.dest('./front-end/1006'));
});

/* Build vendor for Dev */
gulp.task('dev-vendor_1006', ['sass-vendor_1006', 'js-vendor_1006'], function () { });

/* Build not vendor for Dev */
gulp.task('dev-not-vendor_1006', ['jade_1006', 'sass-style_1006', 'js-script_1006', 'copy-asset_1006'], function () {
  // browserSync.init({
  //   server: './front-end/1006',
  //   port: 8090
  // });

  browserSync.init(null, {
    proxy: "http://web1006.me:3000",
    files: ["./front-end/**/*.*"],
    browser: "google chrome",
    port: 3000,
  });

  gulp.watch(['./front-end/1006-source/**/*.jade'], ['jade_1006']);
  gulp.watch(['./front-end/1006-source/**/*.scss'], ['sass-style_1006']);
  gulp.watch(['./front-end/1006-source/**/*.js'], ['js-script_1006']);
  gulp.watch(['./front-end/1006-source/**/*.png', './front-end/1006-source/**/*.jpg', './front-end/1006-source/**/*.svg'], ['copy-asset_1006']);

  console.log('-------------------------');
  console.log('| DEVELOP ENV COMPLETED |');
  console.log('-------------------------');
});

gulp.task('start_1006', function () {
  runSequence('dev-vendor_1006', 'dev-not-vendor_1006', function () {
    console.log('---------------------------');
    console.log('| START PROJECT COMPLETED |');
    console.log('---------------------------');
  });
})

gulp.task('build_1006', ['sass-vendor_1006', 'js-vendor_1006', 'jade_1006', 'sass-style_1006', 'js-script-minify_1006', 'copy-asset_1006'], function () {
  console.log('----------------------------');
  console.log('| PRODUCTION ENV COMPLETED |');
  console.log('----------------------------');
})

/*===================================== 1008 ======================================*/
/*  GULP SASS */
/*  Vendor */
var listScssVendor_1008 = [
  './front-end/1008-source/style/scss/reset.scss',
];

gulp.task('sass-vendor_1008', function () {
  return gulp.src(listScssVendor_1008)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
    .pipe(concat('vendor.min.css'))
    .pipe(gulp.dest('./front-end/1008/style/css/'))
});

/*  Style */
var listScssStyle_1008 = [
  './front-end/1008-source/style/scss/layout.scss',
  './front-end/1008-source/style/scss/component.scss',
  './front-end/1008-source/style/scss/typography.scss',
  './front-end/1008-source/page/**/*.scss',
  './front-end/1008-source/module/**/*.scss'
];
gulp.task('sass-style_1008', function () {
  return gulp.src(listScssStyle_1008)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('./front-end/1008/style/css/'))
    .pipe(reload({ stream: true }));
});

/*  GULP JS */
/*  Vendor */
var listJsVendor_1008 = [
  './node_modules/angular/angular.min.js',
  './node_modules/angular-deferred-bootstrap/angular-deferred-bootstrap.min.js',
  './node_modules/angular-ui-router/release/angular-ui-router.min.js',
];

gulp.task('js-vendor_1008', function () {
  return gulp.src(listJsVendor_1008)
    .pipe(ngAnnotate())
    .pipe(eslint({
      rules: {
        'my-custom-rule': 1,
        'strict': 2
      },
      globals: [
        'jQuery',
        '$'
      ],
      envs: [
        'browser'
      ]
    }))
    .pipe(uglify())
    .pipe(concat('vendor.min.js'))
    .pipe(gulp.dest('./front-end/1008/style/js/'))
});

/*  Script */
var listJsScript_1008 = [
  './front-end/1008-source/style/js/angular-application.js',
  './front-end/1008-source/page/**/*.js',
  './front-end/1008-source/module/**/*.js',
];

gulp.task('js-script_1008', function () {
  return gulp.src(listJsScript_1008)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(ngAnnotate())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./front-end/1008/style/js/'))
    .pipe(reload({ stream: true }));
});

gulp.task('js-script-minify_1008', function () {
  return gulp.src(listJsScript_1008)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./front-end/1008/style/js/'))
    .pipe(reload({ stream: true }));
});

/*  GULP JADE */
gulp.task('jade_1008', function () {
  var locals = {};
  var j = jade({
    locals: locals,
    pretty: false
  }).on('error', function (err) {
    console.log(err);
    j.end();
  });

  return gulp.src('./front-end/1008-source/**/*.jade')
    .pipe(j)
    .pipe(gulp.dest('./front-end/1008'))
    .pipe(reload({ stream: true }));
}
);

/*  GULP COPY FILE */
var listAsset_1008 = [
  './front-end/1008-source/**/*.png',
  './front-end/1008-source/**/*.jpg',
  './front-end/1008-source/**/*.gif',
  './front-end/1008-source/**/*.svg',
  './front-end/1008-source/**/*.eot',
  './front-end/1008-source/**/*.ttf',
  './front-end/1008-source/**/*.woff',
  './front-end/1008-source/**/*.woff2',
  './front-end/1008-source/**/*.otf'
];

gulp.task('copy-asset_1008', function () {
  gulp.src(listAsset_1008)
    .pipe(gulp.dest('./front-end/1008'));
});

/* Build vendor for Dev */
gulp.task('dev-vendor_1008', ['sass-vendor_1008', 'js-vendor_1008'], function () { });

/* Build not vendor for Dev */
gulp.task('dev-not-vendor_1008', ['jade_1008', 'sass-style_1008', 'js-script_1008', 'copy-asset_1008'], function () {
  // browserSync.init({
  //   server: './front-end/1008',
  //   port: 8090
  // });

  browserSync.init(null, {
    proxy: "http://vml.me:3000",
    files: ["./front-end/**/*.*"],
    browser: "google chrome",
    port: 3000,
  });

  gulp.watch(['./front-end/1008-source/**/*.jade'], ['jade_1008']);
  gulp.watch(['./front-end/1008-source/**/*.scss'], ['sass-style_1008']);
  gulp.watch(['./front-end/1008-source/**/*.js'], ['js-script_1008']);
  gulp.watch(['./front-end/1008-source/**/*.png', './front-end/1008-source/**/*.jpg', './front-end/1008-source/**/*.svg'], ['copy-asset_1008']);

  console.log('-------------------------');
  console.log('| DEVELOP ENV COMPLETED |');
  console.log('-------------------------');
});

gulp.task('start_1008', function () {
  runSequence('dev-vendor_1008', 'dev-not-vendor_1008', function () {
    console.log('---------------------------');
    console.log('| START PROJECT COMPLETED |');
    console.log('---------------------------');
  });
})

gulp.task('build_1008', ['sass-vendor_1008', 'js-vendor_1008', 'jade_1008', 'sass-style_1008', 'js-script-minify_1008', 'copy-asset_1008'], function () {
  console.log('----------------------------');
  console.log('| PRODUCTION ENV COMPLETED |');
  console.log('----------------------------');
})
